 angular.module('directive.map', [])
    .directive('mapSelect', ['$timeout', function($timeout) {

            // directive link function
            var link = function(scope, element, attrs, rootScope) {
                // console.log(mapServices.getAllMaincardBordInCountry('IND'));
               // var locationcardboard = mapServices.getAllMaincardBordInCountry('IND');
               // var data = locationcardboard.data;
                var map, infoWindow;
                var markerPin;
                var imagePin = {
                    url: '/images/pin.png',
                    // This marker is 20 pixels wide by 32 pixels high.
                    size: new google.maps.Size(34, 34),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(14, 14)
                };
                var imageBoardIcon = {
                    url: '/images/board.png',
                    // This marker is 20 pixels wide by 32 pixels high.
                    size: new google.maps.Size(34, 34),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(14, 14)
                };
                var cardBoardMarkersA = [];
                var mapOptions = {
                    center: new google.maps.LatLng(0, 0),
                    zoom: 16,
                    scrollwheel: false
                };

                function initMap() {
                    if (map === void 0) {
                        map = new google.maps.Map(element[0], mapOptions);
                        google.maps.event.trigger(map, 'resize');
                    }
                }

                function setMarker(map, position, draggable, title, content, img, metaData) {
                    // console.log('map:' + map + ' ,position:' + position + ',draggable :' + draggable + ',title' + title + ',content:' + content + ',img:' + img);
                    var marker;
                    // alert(position);
                    var markerOptions = {
                        position: position,
                        map: map,
                        draggable: draggable,
                        icon: img,
                        title: title
                    };
                    marker = new google.maps.Marker(markerOptions);
                    marker.setValues(metaData);
                    cardBoardMarkersA.push(marker); // add marker to array

                    marker.addListener('click', function(e) {

                        alert("selected card board:" + marker.get('id'));

                        scope.cardFilter.cardBoardIds = [];
                        scope.cardFilter.cardBoardIds.push(marker.get('id'));
                        scope.$apply();

                        //  console.log(e);
                        // scope.selectBoard=;
                    });



                }

                $timeout(function() {
                    initMap();

                    // console.log(mapServices.getAllMaincardBordInCountry('IND'));
                    var locationcardboard = scope.cardBoards;
                    // var data = locationcardboard.data;
                    // Create the search box and link it to the UI element.
                    var input = document.getElementById('pac-input-card');
                    //alert(input);
                    if (input != null) {
                        var searchBox = new google.maps.places.SearchBox(input);
                        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                        // Bias the SearchBox results towards current map's viewport.
                        map.addListener('bounds_changed', function() {
                            searchBox.setBounds(map.getBounds());
                        });
                        searchBox.addListener('places_changed', function() {

                            var places = searchBox.getPlaces();
                            if (places.length == 0) {
                                return;
                            }
                            // Clear out the old cardBoardMarkersA.
                            cardBoardMarkersA.forEach(function(marker) {
                                marker.setMap(null);
                            });
                            cardBoardMarkersA = [];
                            // For each place, get the icon, name and location.
                            var bounds = new google.maps.LatLngBounds();
                            /*     places.forEach(function(place) {
                                     var icon = {
                                         url: place.icon,
                                         size: new google.maps.Size(71, 71),
                                         origin: new google.maps.Point(0, 0),
                                         anchor: new google.maps.Point(17, 34),
                                         scaledSize: new google.maps.Size(25, 25)
                                     };
                                     if (place.geometry.viewport) {
                                         // Only geocodes have viewport.
                                         bounds.union(place.geometry.viewport);
                                     } else {
                                         bounds.extend(place.geometry.location);
                                     }
                                 });*/
                            map.fitBounds(bounds);
                        });
                    }




                    var metaData;

                   /* for (var i in data) {
                        console.log(data[i].lan);
                        console.log(data[i].lat);
                        metaData = { id: data[i].id };
                        setMarker(map, new google.maps.LatLng(data[i].lat, data[i].lan), false, data[i].place, 'nothing', imageBoardIcon, metaData);
                    }*/
                }, 500);

                scope.heightMap = "400";


            };
            return {
                restrict: 'EA',
                template: '<div id="gmapscard" style="height: 100px" ></div>',
                replace: true,
                link: link,
                controller: function($scope) {
                    $scope.cardFilter = { cardBoardIds: [] };
                }

            };
        }

    ]);

angular.module('starter.addPostCtrl', [])
    .controller('addPostCtrl', function($location, loadingService,$state, $stateParams, $scope, $rootScope,  $timeout, $https) {
        


        $scope.upload = function(file) {
            console.log(file);
            Upload.upload({
                url: '  https://api.imgur.com/3/image',
                method: 'POST', 
                headers: { 'Authorization': 'Client-ID b39bb58fbc698e0', "Content-Type": 'application/json' },
                data: { "image": file }
            }).then(function(resp) {
                console.log('Success ');
                console.log(resp);
                if (resp.status == 200) {
                    console.log(resp.data.data.link);
                    addPostService.uploadImages(resp.data.data.link, $rootScope.postId).then(function(ref) {
                        var id = ref.key;
                        console.log("added record with id " + id);

                        addPostService.getPostImages($rootScope.postId).then(function(result) {

                            $scope.imageByPost = result;
                            console.log(result);

                       }, function(error) {
                            alert(error);
                        });
                        // list.$indexFor(id); // returns location in the array
                    });
                }
            }, function(resp) {
                console.log(resp);
            }, function(evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ');
            });
        };
      
      

    });

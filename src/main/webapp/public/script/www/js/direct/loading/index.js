'use strict';
angular.module('common.loading', [])
    .directive('loadingSpinner', function() {
        return {
            template: '<div class="loadingSpinner" ng-if="isLoadin" ><div class="loadingSpinner-inner"><div class="loadingCenter"><img src="public/script/www/img/loading.gif"  style="width: 100%;"></div> </div></div>',
            restrict: 'EA',
            replace: true,
            controller: 'loadingCtrl'
        };
    })
    .controller('loadingCtrl', function($scope, loadingService) {
       // alert('haiii');

        $scope.isLoadin = loadingService.isLoadin;
         $scope.isLoadin=true;
         //alert($scope.isLoadin);
        

        /* $scope.$watch('loadingService.isLoading', function(newVal) {
             console.log('data changes into: ', newVal)
         }, true);*/

        $scope.$watch(function() {
                return loadingService.isLoadin;
            },

            function(newVal, oldVal) {


               $scope.isLoadin = loadingService.isLoadin;


            }, true);



    })
    .factory('loadingService', function() {
        return {
            isLoading: true
        };
    });

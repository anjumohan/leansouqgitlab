/*angular.module('starter.authService', [])
    .factory('authService', function($rootScope,$firebaseAuth) {*/


angular.module('starter.addPostService', [])
  .factory('addPostService', addPostService);

//.$inject = ['$firebaseAuth', 'firebaseDataService', 'partyService'];
function addPostService(TYPE_POST, $q, $http, MAIN_CONSTANT) {


  return {
    searchdata: searchdata,
    getshops: getshops,
    postreview: postreview,
    getshopsfromuser: getshopsfromuser,
    getproducts: getproducts,
    getShopsMainList: getShopsMainList,
    getProductCategoryList: getProductCategoryList,
    searchdataShop: searchdataShop,
    searchdataProduct: searchdataProduct,
    getallreview: getallreview,
    deleteReview: deleteReview,
    getcategoryfromMain: getcategoryfromMain



  };



  /*function uploadimages(file) {
        var deferred = $q.defer();
      Upload.upload({
       url: '  https://api.imgur.com/3/image',
      method: 'POST', 
      headers: { 'Authorization': 'Client-ID b39bb58fbc698e0', "Content-Type": 'application/json' },
      data:  { "image": file }         
    }).then(function successCallback(response) {
                deferred.resolve(response);
              console.log(response);
          }, function errorCallback(err) {
              deferred.reject("error on upload");
              console.log('called but error', err);
          });
          return deferred.promise;
  }*/




  function searchdataShop(adppostData) {
    var deferred = $q.defer();
    //adppostData
    console.log(adppostData);
    $http({
      method: "POST",
      /* url:MAIN_CONSTANT.site_url+"/input/searchdata",*/
      url: MAIN_CONSTANT.site_url + "input/getshops",
      data: adppostData
    }).then(function mySucces(response) {
        console.log(response);

        deferred.resolve(response.data);

      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }

  function searchdataProduct(adppostData) {
    var deferred = $q.defer();
    //adppostData
    console.log(adppostData);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      /* url:MAIN_CONSTANT.site_url+"/input/searchdata",*/
      url: MAIN_CONSTANT.site_url + "input/getproducts",
      data: adppostData
    }).then(function mySucces(response) {
        console.log(response);
        deferred.resolve(response.data);

      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }



  function getshops(shopId) {
    var deferred = $q.defer();
    //adppostData
    console.log(shopId);
    $http({
      method: "POST",
      /* url:MAIN_CONSTANT.site_url+"/input/getshops?shopId="+shopId+"&mainCategory="+mainCategoryId,*/
      /*   url:"/input/getshops/:shopId&mainCategoryId",*/
      url: MAIN_CONSTANT.site_url + "input/getshops",
      data: shopId
    }).then(function mySucces(response) {
        console.log(response);

        deferred.resolve(response.data);

      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }





  function getshopsfromuser(pagevo) {
    console.log(pagevo);
    var deferred = $q.defer();
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      url: MAIN_CONSTANT.site_url + "input/allshopbyuser",
      data: pagevo
    }).then(function mySucces(response) {
        console.log(response);
        /* if (response.data.errorCode === 0) {
           deferred.resolve(response.data);
         } else {
           deferred.reject(response.data.errorMessage);
         }*/
        deferred.resolve(response.data);

      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }



  function getproducts(shopId) {
    console.log(shopId);
    var deferred = $q.defer();
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "input/allproductsbyuser",
      data: shopId
    }).then(function mySucces(response) {
        console.log(response);
        if (response.data.errorCode === 0) {
          deferred.resolve(response.data);
        } else {
          deferred.reject(response.data.errorMessage);
        }
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }

  function getShopsMainList(maincategoryId) {
    var deferred = $q.defer();
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/input/allshopbyuser",
      data: maincategoryId
    }).then(function mySucces(response) {
        console.log(response);
        if (response.data.errorCode === 0) {
          deferred.resolve(response.data);
        } else {
          deferred.reject(response.data.errorMessage);
        }
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }

  function getProductCategoryList(categoryId) {
    var deferred = $q.defer();
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/input/allproductsbyuser",
      data: categoryId
    }).then(function mySucces(response) {
        console.log(response);
        if (response.data.errorCode === 0) {
          deferred.resolve(response.data);
        } else {
          deferred.reject(response.data.errorMessage);
        }
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }



  function postreview(data) {
    var deffered = $q.defer();
    //adppostData

    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "input/savereview",
      data: data
    }).then(function mySucces(response) {
        console.log(response);
        deffered.resolve(response.data);

        /* if (response.data.errorCode === 0) {
             deffered.resolve(response.data);
         } else {
             deffered.reject("Error message due to");
         }*/
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }


  function getallreview(data) {
    var deffered = $q.defer();
    //adppostData

    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/input/getreviews",
      data: data
    }).then(function mySucces(response) {
        console.log(response);

        deffered.resolve(response.data);

      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }


  function deleteReview(data) {
    var deffered = $q.defer();
    //adppostData

    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/input/deletereview?reviewId=" + data + "&subOrMain=" + 0,

    }).then(function mySucces(response) {
        console.log(response);

        deffered.resolve(response.data);

      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

  function getcategoryfromMain(Mainid) {
    var deffered = $q.defer();
    console.log(Mainid);
    //  console.log(data);
    $http({
      method: "GET",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      /*getsubcategory?mainCategoryId=1&categoryId=1*/
      url: MAIN_CONSTANT.site_url + "input/getcategory?categoryId=0&mainCategoryId=" + Mainid,
      data: Mainid

    }).then(function(data) {
      deffered.resolve(data);
      // alert(data);
      console.log(data);
      //console.log(data.data.body);
    }, function(error) {
      console.log(error);
      deffered.reject("Error message due to -------");


    });
    return deffered.promise;
  }


  function searchdata(adppostData) {
    var deferred = $q.defer();
    //adppostData
    console.log(adppostData);
    $http({
      method: "POST",
      /* url:MAIN_CONSTANT.site_url+"/input/searchdata",*/
      url: MAIN_CONSTANT.site_url + "input/searchdata",
      data: adppostData
    }).then(function mySucces(response) {
        console.log(response);

        deferred.resolve(response.data);

      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }


}

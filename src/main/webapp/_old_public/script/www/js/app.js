  // angular.module is a global place for creating, registering and retrieving Angular modules
  // 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
  // the 2nd parameter is an array of 'requires'
  // 'starter.controllers' is found in controllers.js

  angular.module('starter', [

      'ui.router',
      'common.btnLoading',
      'common.loading',
      
      'starter.homeCtrl',
      
      'starter.authCtrl',
      'starter.categoryCtrl',
       'starter.subcategoryCtrl',
       'starter.productCtrl',
       'starter.customerCtrl',
       'starter.attributeCtrl',
       'starter.auctionCtrl',
       'starter.locationCtrl',
       'starter.sellerCtrl',
       'starter.reportCtrl',
        'starter.taxCtrl',
        'starter.reviewCtrl',
         'starter.orderCtrl',
         'starter.offerCtrl',
         'starter.addCtrl',
         'starter.revenueCtrl',
         'ngMap',

       'starter.authService',
       'starter.inputService',
       'starter.auctionService',
       'starter.storeCtrl',
       'starter.promocodeCtrl',
       'starter.loadingService',


       'starter.storeService',
       'starter.sellerService',
       'ngStorage',
       'textAngular',
     
      'ui.bootstrap',
      'ngFileUpload',
      'starter.inputCtrl',
      /*  'angularFileUpload',*/
      'ngTable',
      'constant',
      'toaster',
      // 'htmlToPdfSave',
      'chart.js',     
      // 'angular-search-and-select'



    ])

    .run(function($templateCache, $rootScope, $state, loadingService) {
    })

    .config(function($stateProvider, $urlRouterProvider) {
      $stateProvider


        .state('login', {
          url: '/login',
          templateUrl: 'public/script/www/templates/auth/login.html',
          controller: 'authCtrl'
        })


        .state('app', {
          url: '/app',
          
          templateUrl: 'public/script/www/templates/auth/login.html',
          controller: 'authCtrl'
        })
        .state('admin', {
          url: '/admin',
          abstract: true,
                    
          templateUrl: 'public/script/www/templates/common/sidebar.html',
          controller: 'homeCtrl'
        })

        .state('admin.dashboard', {
          url: '/dashboard',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/dashboard/dashboard.html',
              controller: 'homeCtrl'
            }
          }

        })
           .state('admin.cmsdata', {
          url: '/cmsdata',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/cms-data/cms-data.html',
              controller: 'categoryCtrl'
            }
          }


        })

        //  .state('admin.advertisement', {
        //   url: '/advertisement',
        //   views: {
        //     'menuContent': {
        //       templateUrl: 'public/script/www/templates/advertisement.html',
        //       controller: 'advertisementCtrl'
        //     }
        //   }

        // })
// REPORTS
 .state('admin.userreports', {
          url: '/userreports',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/user-reports.html',
              controller: 'reportCtrl'
            }
          }

        })
 .state('admin.lowstockreport', {
          url: '/lowstockreport',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/low-stock-report.html',
              controller: 'reportCtrl'
            }
          }

        })
  .state('admin.orderreports', {
          url: '/orderreports',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/order-reports.html',
              controller: 'reportCtrl'
            }
          }

        })
  .state('admin.productreport', {
          url: '/productreport',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/product-report.html',
              controller: 'reportCtrl'
            }
          }

        })
  .state('admin.best-purchased-productreport', {
          url: '/best-purchased-productreport',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/best-purchased-product-report.html',
              controller: 'reportCtrl'
            }
          }

        })
    .state('admin.sales-report', {
          url: '/sales-report',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/sales-report.html',
              controller: 'reportCtrl'
            }
          }

        })
    .state('admin.transaction-report', {
          url: '/transaction-report',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reports/transaction-report.html',
              controller: 'reportCtrl'
            }
          }

        })

    // Manage Revenue
     .state('admin.managerevenue', {
          url: '/managerevenue',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/revenue/manage-revenue.html',
              controller: 'revenueCtrl'
            }
          }

        })

 .state('admin.promocode', {
          url: '/promocode',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/promocode/promocodemanagement.html',
              controller: 'promocodeCtrl'
            }
          }

        })
     .state('admin.addpromocode', {
          url: '/addpromocode/:codeId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/promocode/add-promocode.html',
              controller: 'promocodeCtrl'
            }
          }

        })
       .state('admin.viewpromocode', {
          url: '/viewpromocode/:codeId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/promocode/view-promocode.html',
              controller: 'promocodeCtrl'
            }
          }

        })
          .state('admin.viewordersbypromocode', {
          url: '/viewordersbypromocode/:codeId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/promocode/view-ordersby-promocode.html',
              controller: 'promocodeCtrl'
            }
          }

        })
        .state('admin.viewpromoter', {
          url: '/viewpromoter/:customerId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/promocode/view-promoter.html',
              controller: 'promocodeCtrl'
            }
          }

        })
          .state('admin.savepromoter', {
          url: '/savepromoter/:customerId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/promocode/save-promoter.html',
              controller: 'promocodeCtrl'
            }
          }

        })

          .state('admin.category', {
          url: '/category',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/category/categorymanagement.html',
              controller: 'categoryCtrl'
            }
          }

        })
          .state('admin.addcategory', {
          url: '/addcategory/:categoryId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/category/addcategory.html',
              controller: 'categoryCtrl'
            }
          }

        })
           .state('admin.viewcategory', {
          url: '/viewcategory/:categoryId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/category/viewcategory.html',
              controller: 'categoryCtrl'
            }
          }

        })

           .state('admin.deactivatedcategories', {
          url: '/deactivatedcategories',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/category/deactivatedcategories.html',
              controller: 'categoryCtrl'
            }
          }

        })

           .state('admin.subcategory', {
          url: '/subcategory',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/subcategory/itemtype.html',
              controller: 'subcategoryCtrl'
            }
          }


        })
        
        .state('admin.addsubcategory', {
          url: '/addsubcategory/:subCategoryId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/subcategory/additemtype.html',
              controller: 'subcategoryCtrl'
            }
          }
          

        })


            .state('admin.viewsubcategory', {
          url: '/viewsubcategory/:subCategoryId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/subcategory/viewsubcategory.html',
              controller: 'subcategoryCtrl'
            }
          }

        })

           .state('admin.deactivatedsubcategories', {
          url: '/deactivatedsubcategories',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/subcategory/deactivatedsubcategories.html',
              controller: 'subcategoryCtrl'
            }
          }

        })


           .state('admin.country', {
          url: '/country',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/country/countrymanagement.html',
              controller: 'locationCtrl'
            }
          }

        })
          .state('admin.addcountry', {
          url: '/addcountry/:countryId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/country/addcountry.html',
              controller: 'locationCtrl'
            }
          }

        })

          .state('admin.viewcountry', {
          url: '/viewcountry/:countryId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/country/viewcountry.html',
              controller: 'locationCtrl'
            }
          }

        })




          .state('admin.city', {
          url: '/city',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/city/citymanagement.html',
              controller: 'locationCtrl'
            }
          }

        })
          .state('admin.addcity', {
          url: '/addcity/:cityId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/city/addcity.html',
              controller: 'locationCtrl'
            }
          }

        })

           .state('admin.viewcity', {
          url: '/viewcity/:cityId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/city/viewcity.html',
              controller: 'locationCtrl'
            }
          }

        })
              .state('admin.myaccountsettings', {
          url: '/myaccountsettings',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/account-settings/account-settings.html',
              controller: 'categoryCtrl'
            }
          }

        })

           // attribute

           .state('admin.attribute', {
          url: '/attribute',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/attribute/attributes.html',
              controller: 'attributeCtrl'
            }
          }

        })
         .state('admin.deactivatedattribute', {
          url: '/deactivatedattribute',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/attribute/deactivatedattributes.html',
              controller: 'attributeCtrl'
            }
          }

        })

        .state('admin.addattribute', {
          url: '/addattribute/:attributeId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/attribute/addattribute.html',
              controller: 'attributeCtrl'
            }
          }

        })

         .state('admin.viewattribute', {
          url: '/viewattribute/:attributeId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/attribute/viewattribute.html',
              controller: 'attributeCtrl'
            }
          }

        })
         .state('admin.reviews', {
          url: '/reviews/:productId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/reviews/reviewsandratings.html',
              controller: 'reviewCtrl'
            }
          }

        })

//product
.state('admin.stock', {
          url: '/stock',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/product/productstock.html',
              controller: 'productCtrl'
            }
          }

        })
.state('admin.addstock', {
          url: '/addstock/:productId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/product/addstock.html',
              controller: 'productCtrl'
            }
          }

        })
    .state('admin.product', {
          url: '/product',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/product/itemmanagement.html',
              controller: 'productCtrl'
            }
          }

        })

        .state('admin.addproduct', {
          url: '/addproduct/:productId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/product/additem.html',
              controller: 'productCtrl'
            }
          }

        })
          .state('admin.assignofferonproduct', {
          url: '/assignofferonproduct/:productId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/product/assign-offer-on-product.html',
              controller: 'productCtrl'
            }
          }

        })

        .state('admin.viewproduct', {
          url: '/viewproduct/:productId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/product/viewitem.html',
              controller: 'productCtrl'
            }
          }

        })

        //SELLER
         .state('admin.seller', {
          url: '/seller',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/seller/sellermanagement.html',
              controller: 'sellerCtrl'
            }
          }

        })
       


         .state('admin.addseller', {
          url: '/addseller/:sellerId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/seller/addseller.html',
              controller: 'sellerCtrl'
            }
          }

        })
          .state('admin.viewseller', {
          url: '/viewseller/:sellerId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/seller/viewseller.html',
              controller: 'sellerCtrl'
            }
          }

        })
           .state('admin.editseller', {
          url: '/editseller',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/seller/editseller.html',
              controller: 'sellerCtrl'
            }
          }

        })

          .state('admin.sellerstatistics', {
          url: '/sellerstatistics',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/seller/sellerstatistics.html',
              controller: 'sellerCtrl'
            }
          }

        })
      


// CUSTOMER
 .state('admin.customer', {
          url: '/customer',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/customeraccounts.html',
              controller: 'customerCtrl'
            }
          }

        })
          .state('admin.viewcustomer', {
          url: '/viewcustomer/:customerId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/viewcustomer.html',
              controller: 'customerCtrl'
            }
          }

        })
          .state('admin.addcustomer', {
          url: '/addcustomer/:customerId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/addcustomer.html',
              controller: 'customerCtrl'
            }
          }

        })
          .state('admin.editcustomer', {
          url: '/editcustomer/:customerId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/editcustomer.html',
              controller: 'customerCtrl'
            }
          }

        })

          .state('admin.customerstatistics', {
          url: '/customerstatistics',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/customerstatistics.html',
              controller: 'customerCtrl'
            }
          }

        })

          .state('admin.customerreview', {
          url: '/customerreview',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/customerreview.html',
              controller: 'customerCtrl'
            }
          }

        })
 

           .state('admin.tax', {
          url: '/tax',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/tax/taxmanagement.html',
              controller: 'taxCtrl'
            }
          }

        })
           .state('admin.addtax', {
          url: '/addtax/:taxId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/tax/addtax.html',
              controller: 'taxCtrl'
            }
          }

        })
           .state('admin.viewtax', {
          url: '/viewtax/:taxId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/tax/viewtax.html',
              controller: 'taxCtrl'
            }
          }

        })
           // OFFER
              .state('admin.offer', {
          url: '/offer',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/offer/offer.html',
              controller: 'offerCtrl'
            }
          }

        })

                 .state('admin.addoffer', {
          url: '/addoffer/:offerId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/offer/addoffer.html',
              controller: 'offerCtrl'
            }
          }

        })
           .state('admin.viewoffer', {
          url: '/viewoffer/:offerId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/offer/viewoffer.html',
              controller: 'offerCtrl'
            }
          }

        })
            .state('admin.listofproducts', {
          url: '/listofproducts',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/offer/listofproducts.html',
              controller: 'offerCtrl'
            }
          }

        })
              .state('admin.homedata', {
          url: '/homedata',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/homedata/homedatamanagement.html',
              controller: 'addCtrl'
            }
          }

        })
          .state('admin.addslider', {
          url: '/addslider/:addId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/homedata/addslider.html',
              controller: 'addCtrl'
            }
          }

        })
           .state('admin.addbanner', {
          url: '/addbanner/:addId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/homedata/addbanner.html',
              controller: 'addCtrl'
            }
          }

        })
           // RETURNS
           .state('admin.returns', {
          url: '/returns',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/returns/return-order.html',
              controller: 'orderCtrl'
            }
          }

        })
  // .state('admin.viewreturn', {
  //         url: '/viewreturn/:orderId',
  //         views: {
  //           'menuContent': {
  //             templateUrl: 'public/script/www/templates/returns/view-return.html',
  //             controller: 'orderCtrl'
  //           }
  //         }

  //       })

.state('admin.viewreturn', {
          url: '/viewreturn/:orderId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/returns/view-returnorder.html',
              controller: 'orderCtrl'
            }
          }

        })


           // MANUFACTURE
           .state('admin.manufacture', {
          url: '/manufacture',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/manufacture/manufacture.html',
              controller: 'categoryCtrl'
            }
          }

        })
            .state('admin.addmanufacture', {
          url: '/addmanufacture/:manufacturerId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/manufacture/addmanufacture.html',
              controller: 'categoryCtrl'
            }
          }

        })
             .state('admin.viewmanufacture', {
          url: '/viewmanufacture/:manufacturerId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/manufacture/viewmanufacture.html',
              controller: 'categoryCtrl'
            }
          }

        })
//ORDER
.state('admin.order', {
          url: '/order',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/order.html',
              controller: 'orderCtrl'
            }
          }

        })
.state('admin.neworder', {
          url: '/neworder',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/order/neworders.html',
              controller: 'orderCtrl'
            }
          }

        })
// .state('seller.neworder', {
//           url: '/neworder',
//           views: {
//             'menuContent': {
//               templateUrl: 'public/script/www/templates/order/neworders.html',
//               controller: 'orderCtrl'
//             }
//           }

//         })

.state('admin.vieworder', {
          url: '/vieworder/:orderId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/order/vieworder.html',
              controller: 'orderCtrl'
            }
          }

        })
.state('admin.editorder', {
          url: '/editorder/:orderId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/order/editorder.html',
              controller: 'orderCtrl'
            }
          }

        })

.state('admin.milestoneorder', {
          url: '/milestoneorder/:orderId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/milestoneoforder.html',
              controller: 'orderCtrl'
            }
          }

        })
.state('admin.deliveredorders', {
          url: '/deliveredorders',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/deliveredorders.html',
              controller: 'orderCtrl'
            }
          }

        })
.state('admin.invoice', {
          url: '/invoice/:orderId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/invoice.html',
              controller: 'orderCtrl'
            }
          }

        })
          
       
          .state('admin.itemacceptance', {
          url: '/itemacceptance/:requestItemId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/itemacceptance.html',
              controller: 'storeCtrl'
            }
          }})

            .state('admin.itemdeactivation', {
          url: '/itemdeactivation',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/itemdeactivation.html',
              controller: 'storeCtrl'
            }
          }

        })
          .state('admin.responsedstorerequests', {
          url: '/responsedstorerequests',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/responsedstoreitemrequests.html',
              controller: 'storeCtrl'
            }
          }

        })
          .state('admin.listofcustomerorder', {
          url: '/listofcustomerorder/:userId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/listofordersbycustomer.html',
              controller: 'customerCtrl'
            }
          }

        })
          .state('admin.customerorderdetails', {
          url: '/customerorderdetails/:orderId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customerorderdetails.html',
              controller: 'customerCtrl'
            }
          }

        })
          .state('admin.listofstoreorder', {
          url: '/listofstoreorder/:storeId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/listofordersbystore.html',
              controller: 'storeCtrl'
            }
          }

        })


        .state('app.signup', {
          url: '/signup',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/auth/signup.html',
              controller: 'authCtrl'
            }
          }

        })

       
       .state('app.home', {
          url: '/home?mode',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/common/sidebar.html',
              controller: 'homeCtrl'
            }
          }

        })

       
      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/login');
    });

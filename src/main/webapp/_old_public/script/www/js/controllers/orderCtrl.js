angular.module('starter.orderCtrl', [])
  .controller('orderCtrl', function($scope, $window, $state, inputService, toaster, sellerService, FILE_CONSTANT, toaster, MAIN_CONSTANT, Upload, sellerService, $stateParams, $localStorage, loadingService) {

    if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
    $window.scrollTo(0, 0);
    $scope.imageUrl = FILE_CONSTANT.filepath;
    $scope.sellerId = $localStorage.sellerId;
    console.log($scope.sellerId);

    // start new order page

    if ($state.current.name === 'admin.returns') {
      // if( $localStorage.roleId !== 50){
      //     $window.location.href ='#/login'

      //   }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }

      // alert("hai");
      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;

      $scope.getallproductreturns = function(pageno) {
        $scope.orderreturnPageVo = {
          "returnItemId": 0,
          "rowPerPage": 25,
          "currentIndex": 0,
          "stat": 10,
          "storeId": 0
        }

        // {
        //    "rowPerPage":25,
        //    "currentIndex":0,
        //    "customerId":0,
        //    "fromDate":0,"toDate":0,"orderId":0,"status":[2],
        //    "storeId":$localStorage.currentloginstoreId,
        //   "deliveryDate":"0","serviceOrProduct":"",
        //    "deliveryBoyId":0,"orderListId":"0"
        //  }
        console.log($scope.orderreturnPageVo);
        $scope.orderreturnPageVo.currentIndex = (pageno - 1) * $scope.orderreturnPageVo.rowPerPage;
        loadingService.isLoadin = true;

        sellerService.getallreturns($scope.orderreturnPageVo).then(function(response) {
          loadingService.isLoadin = false;

          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderreturnPageVo.rowPerPage);
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.allreturnorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.allreturnorders);
            $scope.noorderstoship = 0;


          } else if (response.errorCode === 2) {
            $scope.allreturnorders = [];
            loadingService.isLoadin = false;
            $scope.noorderstoship = 1;




          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            // alert("helllo");
           // alert(response.errorMessage);
          }
        });
      }
      $scope.getallproductreturns($scope.pageno);

      // confirmed returns
      $scope.getallconfirmedreturns = function(pageno) {
        $scope.orderreturnconfirmPageVo = {
          "returnItemId": 0,
          "rowPerPage": 25,
          "currentIndex": 0,
          "stat": 11,
          "storeId": 0
        }

        // {
        //    "rowPerPage":25,
        //    "currentIndex":0,
        //    "customerId":0,
        //    "fromDate":0,"toDate":0,"orderId":0,"status":[2],
        //    "storeId":$localStorage.currentloginstoreId,
        //   "deliveryDate":"0","serviceOrProduct":"",
        //    "deliveryBoyId":0,"orderListId":"0"
        //  }
        console.log($scope.orderreturnconfirmPageVo);
        $scope.orderreturnconfirmPageVo.currentIndex = (pageno - 1) * $scope.orderreturnconfirmPageVo.rowPerPage;
        loadingService.isLoadin = true;

        sellerService.getallreturns($scope.orderreturnconfirmPageVo).then(function(response) {
          loadingService.isLoadin = false;

          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderreturnconfirmPageVo.rowPerPage);
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.allacceptedreturs = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.allacceptedreturs);
            $scope.noallacceptedreturs = 0;


          } else if (response.errorCode === 2) {
            $scope.allacceptedreturs = [];
            loadingService.isLoadin = false;
            $scope.noallacceptedreturs = 1;




          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            // alert("helllo");
            //alert(response.errorMessage);
          }
        });
      }
      // Rejected returns
      $scope.getallrejectedreturns = function(pageno) {
        $scope.orderreturnrejectedPageVo = {
          "returnItemId": 0,
          "rowPerPage": 25,
          "currentIndex": 0,
          "stat": 12,
          "storeId": $localStorage.currentloginstoreId
        }

        // {
        //    "rowPerPage":25,
        //    "currentIndex":0,
        //    "customerId":0,
        //    "fromDate":0,"toDate":0,"orderId":0,"status":[2],
        //    "storeId":$localStorage.currentloginstoreId,
        //   "deliveryDate":"0","serviceOrProduct":"",
        //    "deliveryBoyId":0,"orderListId":"0"
        //  }
        console.log($scope.orderreturnrejectedPageVo);
        $scope.orderreturnrejectedPageVo.currentIndex = (pageno - 1) * $scope.orderreturnrejectedPageVo.rowPerPage;
        loadingService.isLoadin = true;

        sellerService.getallreturns($scope.orderreturnrejectedPageVo).then(function(response) {
          loadingService.isLoadin = false;

          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderreturnrejectedPageVo.rowPerPage);
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.allrejectedreturs = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.allrejectedreturs);
            $scope.noallallrejectedreturs = 0;


          } else if (response.errorCode === 2) {
            loadingService.isLoadin = false;
            $scope.noallallrejectedreturs = 1;




          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            // alert("helllo");
           // alert(response.errorMessage);
          }
        });
      }

      // COMPLETED RETURNS

      $scope.getallcompletedreturns = function(pageno) {
        $scope.orderreturncompletedPageVo = {
          "returnItemId": 0,
          "rowPerPage": 25,
          "currentIndex": 0,
          "stat": 15,
          "storeId": $localStorage.currentloginstoreId
        }

        // {
        //    "rowPerPage":25,
        //    "currentIndex":0,
        //    "customerId":0,
        //    "fromDate":0,"toDate":0,"orderId":0,"status":[2],
        //    "storeId":$localStorage.currentloginstoreId,
        //   "deliveryDate":"0","serviceOrProduct":"",
        //    "deliveryBoyId":0,"orderListId":"0"
        //  }
        console.log($scope.orderreturncompletedPageVo);
        $scope.orderreturncompletedPageVo.currentIndex = (pageno - 1) * $scope.orderreturncompletedPageVo.rowPerPage;
        loadingService.isLoadin = true;

        sellerService.getallreturns($scope.orderreturncompletedPageVo).then(function(response) {
          loadingService.isLoadin = false;

          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderreturncompletedPageVo.rowPerPage);
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.allcompletedreturs = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.allcompletedreturs);
            $scope.noallcompletedreturs = 0;


          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === 2) {
            loadingService.isLoadin = false;
            $scope.noallcompletedreturs = 1;




          } else {
            // alert("helllo");
          //  alert(response.errorMessage);
          }
        });
      }
      //        $scope.getallconfirmedreturns= function(pageno) {
      //          $scope.confirmedreturnPageVo ={
      // "returnItemId":0,"rowPerPage":25,"currentIndex":0,"storeId":$localStorage.currentloginstoreId
      // }

      //       // {
      //       //    "rowPerPage":25,
      //       //    "currentIndex":0,
      //       //    "customerId":0,
      //       //    "fromDate":0,"toDate":0,"orderId":0,"status":[2],
      //       //    "storeId":$localStorage.currentloginstoreId,
      //       //   "deliveryDate":"0","serviceOrProduct":"",
      //       //    "deliveryBoyId":0,"orderListId":"0"
      //       //  }
      // $scope.confirmedreturnPageVo.currentIndex = (pageno - 1) * $scope.confirmedreturnPageVo.rowPerPage;
      //          loadingService.isLoadin=true;

      //         sellerService.getallreturns($scope.confirmedreturnPageVo).then(function(response) {
      //            loadingService.isLoadin=false;

      //               $scope.noPages = Math.ceil(response.totalRecords / $scope.confirmedreturnPageVo.rowPerPage);
      //           console.log(response);

      //           //  if (response.errorCode === 0) {
      //           //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
      //           //       $scope.stores = response.object.Store;
      //           // console.log($scope.stores );

      //           if (response.errorCode === 0) {
      //             // $scope.shopList = $scope.shopList.concat(response.object.Product);

      //             $scope.confirmedreturnorders = response.object;
      //              // $scope.orderdetails = response.object;
      //              // [0].orderDetailsVo;
      //              console.log($scope.confirmedreturnorders);
      //              $scope.noorderstoship=0;


      //           } else if (response.errorCode === -3){
      //              loadingService.isLoadin=false;
      //               $scope.noorderstoship=1;




      //           }else {
      //             // alert("helllo");
      //             alert(response.errorMessage);
      //           }
      //         });
      //       }



    }
    //  if ($state.current.name === 'seller.orderstoshipping') {
    //   if (!$localStorage.token || $localStorage.token === 0) {
    //       $window.location.href = '#/login';
    //     }

    // // alert("hai");
    //      $scope.noPages = 1;
    //       $scope.getNumber = function(num) {
    //         return new Array(num);
    //       }


    //       $scope.pageno = 1;

    //       $scope.getallorderstoshipping = function(pageno) {
    //          $scope.orderPageVo =

    //       {
    //          "rowPerPage":25,
    //          "currentIndex":0,
    //          "customerId":0,
    //          "fromDate":0,"toDate":0,"orderId":0,"status":[2],
    //          "storeId":$localStorage.currentloginstoreId,
    //         "deliveryDate":"0","serviceOrProduct":"",
    //          "deliveryBoyId":0,"orderListId":"0"
    //        }
    // $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
    //          loadingService.isLoadin=true;

    //         sellerService.getallorders($scope.orderPageVo).then(function(response) {
    //            loadingService.isLoadin=false;

    //               $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
    //           console.log(response);

    //           //  if (response.errorCode === 0) {
    //           //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
    //           //       $scope.stores = response.object.Store;
    //           // console.log($scope.stores );

    //           if (response.errorCode === 0) {
    //             // $scope.shopList = $scope.shopList.concat(response.object.Product);

    //             $scope.orderstoship = response.object;
    //              // $scope.orderdetails = response.object;
    //              // [0].orderDetailsVo;
    //              console.log($scope.orderstoship);
    //              $scope.noorderstoship=0;


    //           } else if (response.errorCode === -3){
    //              loadingService.isLoadin=false;
    //               $scope.noorderstoship=1;




    //           }else {
    //             // alert("helllo");
    //             alert(response.errorMessage);
    //           }
    //         });
    //       }
    //       $scope.getallorderstoshipping($scope.pageno);


    //   }
    if ($state.current.name === 'admin.neworder') {
      // if( $localStorage.roleId !== 50){
      //     $window.location.href ='#/login'

      //   }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }

      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;

      $scope.getallorders = function(pageno) {
        $scope.orderPageVo =

          {
            "rowPerPage": 25,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": 0,
            "status": [1],
            "storeId": 0,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }
        console.log($scope.orderPageVo);
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        // loadingService.isLoadin=true;
        $scope.loading = 1;

        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          // loadingService.isLoadin=false;
          $scope.loading = 0;

          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.neworders = response.object;
            console.log($scope.neworders);
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            // console.log($scope.orderdetails);
            $scope.nodata = 0;


          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            $scope.loading = 0;
            // loadingService.isLoadin=false;
            $scope.nodata = 1;




          } else {
            // alert("helllo");
           // alert(response.errorMessage);
          }
        });
      }
      $scope.getallorders($scope.pageno);

      // confirmed orders

      $scope.getallconfirmedorders = function(pageno) {
        $scope.orderPageVo =

          {
            "rowPerPage": 25,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": 0,
            "status": [2],
            "storeId": $localStorage.currentloginstoreId,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.confirmedorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.confirmedorders);
            $scope.noconfirmeddata = 0;

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            // loadingService.isLoadin=false;
            $scope.loading = 0;
            $scope.noconfirmeddata = 1;




          } else {
          //  alert(response.errorMessage);
          }
        });
      }
      // $scope.getallconfirmedorders($scope.pageno);


      $scope.getallrejectedorders = function(pageno) {
        $scope.orderPageVo =

          {
            "rowPerPage": 25,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": 0,
            "status": [3],
            "storeId": $localStorage.currentloginstoreId,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.rejectedorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.rejectedorders);
            $scope.norejecteddata = 0;
          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            // loadingService.isLoadin=false;
            $scope.loading = 0;
            $scope.norejecteddata = 1;
          } else {
           // alert(response.errorMessage);
          }
        });
      }
      // $scope.getallrejectedorders($scope.pageno);

      $scope.getallshippedorders = function(pageno) {
        $scope.shippedorderPageVo =

          {
            "rowPerPage": 25,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": 0,
            "status": [10],
            "storeId": $localStorage.currentloginstoreId,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.shippedorderPageVo.currentIndex = (pageno - 1) * $scope.shippedorderPageVo.rowPerPage;
        sellerService.getallorders($scope.shippedorderPageVo).then(function(response) {
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          $scope.noPages = Math.ceil(response.totalRecords / $scope.shippedorderPageVo.rowPerPage);
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.shippedorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.shippedorders);
            $scope.noshippedordersdata = 0;

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            // loadingService.isLoadin=false;
            $scope.loading = 0;
            $scope.noshippedordersdata = 1;




          } else {
          //  alert(response.errorMessage);
          }
        });
      }



      $scope.getalldeliveredorders = function(pageno) {
        $scope.orderPageVo =

          {
            "rowPerPage": 25,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": 0,
            "status": [4],
            "storeId": $localStorage.currentloginstoreId,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.deliveredorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.deliveredorders);
            $scope.nodelivereddata = 0;
          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            // loadingService.isLoadin=false;
            $scope.loading = 0;
            $scope.nodelivereddata = 1;
            console.log($scope.nodelivereddata);
          } else {
           // alert(response.errorMessage);
          }
        });
      }

      $scope.getallpickedorders = function(pageno) {
        $scope.pickedorderPageVo =

          {
            "rowPerPage": 25,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": 0,
            "status": [6],
            "storeId": $localStorage.currentloginstoreId,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.pickedorderPageVo.currentIndex = (pageno - 1) * $scope.pickedorderPageVo.rowPerPage;
        sellerService.getallorders($scope.pickedorderPageVo).then(function(response) {
          $scope.noPages = Math.ceil(response.totalRecords / $scope.pickedorderPageVo.rowPerPage);
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.pickedorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.pickedorders);
            $scope.nopickeddata = 0;
          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            // loadingService.isLoadin=false;
            $scope.loading = 0;
            $scope.nopickeddata = 1;
            console.log($scope.nopickeddata);
          } else {
           // alert(response.errorMessage);
          }
        });
      }
      // $scope.getalldeliveredorders($scope.pageno);




    }



    //end new order page

    if ($state.current.name === 'admin.order') {
      if ($localStorage.roleId !== 50) {
        $window.location.href = '#/login'

      }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }

      $scope.orderPageVo = {
        "rowPerPage": 100,
        "currentIndex": 0,
        "customerId": 0,
        "fromDate": 0,
        "toDate": 0,
        "orderId": 0,
        "status": [1],
        "sellerId": 0,
        "deliveryDate": "0",
        "serviceOrProduct": "0",
        "orderListId": "0"
      }

      $scope.getallorders = function() {
        loadingService.isLoadin = true;

        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.orders = response.object;
            $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.orderdetails);

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            //alert(response.errorMessage);
          }
        });
      }
      $scope.getallorders();



    }
    // ########################

    if ($state.current.name === 'admin.deliveredorders') {
      if ($localStorage.roleId !== 50) {
        $window.location.href = '#/login'

      }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }

      $scope.orderPageVo = {
        "rowPerPage": 9,
        "currentIndex": 0,
        "customerId": 0,
        "fromDate": 0,
        "toDate": 0,
        "orderId": 0,
        "status": [1],
        "sellerId": 0,
        "deliveryDate": "0",
        "serviceOrProduct": "0",
        "orderListId": "0"
      }

      $scope.getallorders = function() {
        loadingService.isLoadin = true;

        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.orders = response.object;
            $scope.orderdetails = response.object[0].orderDetailsVo;
            console.log($scope.orderdetails);

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
           // alert(response.errorMessage);
          }
        });
      }
      $scope.getallorders();



    }
    // ##################
    $scope.pop = function() {
      toaster.pop('info', "title", "text");
    };


    $scope.store = {};
    $scope.store.featureOrPhotos = [];

    $scope.upload = function(file) {

      //$scope.isFileuploadStart=true;
      $scope.storelogUploading = true;


      if (file) {

        Upload.upload({
          url: MAIN_CONSTANT.site_url + "/api/uploadtoaws",
          headers: {
            "Content-Type": 'multipart/*',
            Bearer: $localStorage.token
          },
          /*data: { file: FILE_CONSTANT.site_url + file.name,
            type: 'image/jpeg' }*/
          data: {
            file: file
          }
        }).then(function(resp) {
          $scope.store.imageName = resp.data.object;
          $scope.storelogUploading = false;

          console.log(resp);




        }, function(resp) {
          $scope.progressPercentage = 0;


        }, function(evt) {

          $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
          console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
          // $scope.file = null;

        });
      }
    };






    // get store statistics*/
    // ViewOrderdetails

    $scope.ViewOrderdetails = function(order) {
      loadingService.isLoadin = true;
      console.log(order);
      console.log(order.orderId);

      $window.location.href = '#/seller/invoice/' + order.orderId;
      $localStorage.order = order;
      loadingService.isLoadin = false;

      // $scope.getallordersbyuserId(customer.userId);


    };
    if ($state.current.name === 'seller.invoice') {
      if ($localStorage.roleId !== 50) {
        $window.location.href = '#/login'

      }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }
      console.log($stateParams.orderId);
    //  alert($stateParams.orderId);
      $scope.Id = $stateParams.orderId;
      console.log($scope.Id);

      //     $scope.orderPageVo = {
      //       "orderId":$scope.Id,
      //       "orderDetailsId":"0",
      //       "customerId":0,
      //       "fromDate":0,
      //       "toDate":0,"sellerId":0,
      //       "currentIndex":0,
      //       "rowPerPage":20
      // }
      $scope.orderPageVo = {
        "rowPerPage": 100,
        "currentIndex": 0,
        "customerId": 0,
        "fromDate": 0,
        "toDate": 0,
        "orderId": 0,
        "status": [1],
        "sellerId": 0,
        "deliveryDate": "0",
        "serviceOrProduct": "0",
        "orderListId": "0"
      }

      $scope.getallorders = function() {

        loadingService.isLoadin = true;

        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.orders = response.object;
            $scope.orderdetails = response.object[0];
            $scope.transaction = response.object[0].transaction;
            var customerId = response.object[0].customerId;
            var sellerIdId = response.object[0].sellerId;
            console.log($scope.orderdetails);
            console.log(customerId);
            console.log($scope.transaction);
            $scope.getallcustomers(customerId);
            $scope.getseller(sellerIdId);

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
           // alert(response.errorMessage);
          }
        });
        // console.log($scope.customerId);

      }
      $scope.getallorders();

      // $scope.getcustomer = function() {





      // }
      //  $scope.getcustomer();

      //  ##########


      //get seller details


      // alert(33333333);
      $scope.getseller = function(sellerIdId) {
        $scope.sellerPageVo = {
          "sellerId": sellerIdId,
          "sellerName": "",
          "phoneNumber": "",
          "address": "",
          "status": [0],
          "currentIndex": 0,
          "rowPerPage": 100
        }
        loadingService.isLoadin = true;

        sellerService.getallsellers($scope.sellerPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.sellers = response.object[0];
            console.log($scope.sellers);



          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
           // alert(response.errorMessage);
          }
        }, function() {
         // alert('Oops! somthing went wrong');
        });
      }
      // $scope.getseller();




      //end get seller details



      // alert(33333333);
      $scope.getallcustomers = function(customerId) {
        console.log(customerId);
        $scope.customerPageVo = {
          "customerId": 0,
          "cityId": 0,
          "countryId": 0,
          "userId": customerId,
          "orderBy": 0,
          "fromDate": 0,
          "toDate": 0,
          "currentIndex": 0,
          "rowPerPage": 100,
          "status": 2
        }
        console.log($scope.customerPageVo);

        loadingService.isLoadin = true;

        inputService.getallcustomers($scope.customerPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.customers = response.object;
            console.log($scope.customers);



          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
           // alert(response.errorMessage);
          }
        }, function() {
       //   alert('Oops! somthing went wrong');
        });
      };
      $scope.getallcustomers();





    }






    if ($state.current.name === 'seller.listofstoreorder') {
      if ($localStorage.roleId !== 50) {
        $window.location.href = '#/login'

      }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }

      $scope.pageno = 1;
      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        console.log(num);
        return new Array(num);
      }
      // alert('323');

      console.log($stateParams.storeId);
      $scope.Id = $stateParams.storeId;
      console.log($scope.Id);
      $scope.orderPageVo = {
        "rowPerPage": 15,
        "currentIndex": 0,
        "customerId": 0,
        "fromDate": 0,
        "toDate": 0,
        "orderId": 0,
        "status": [2, 4, 6],
        "storeId": $scope.Id

      }

      $scope.getallordersbystoreId = function(pageno) {
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        console.log($stateParams.storeId);




        console.log($scope.orderPageVo);
        sellerService.getallordersbystoreId($scope.orderPageVo).then(function(response) {
          loadingService.isLoadin = false;
          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          console.log(response);
          if (response.errorCode === 0) {
            $scope.ordersbystoreId = response.object;
            console.log($scope.ordersbystoreId);
          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          };
        }, function() {
          loadingService.isLoadin = false;
        });
      };
      $scope.getallordersbystoreId($scope.pageno);

    }





    $scope.changeorderstatus = function(order, changeId) {
      // alert(order);
      loadingService.isLoadin = true;
      console.log(order, changeId);
      $scope.orderId = order.orderId;
      console.log($scope.orderId);
      // $window.location.href = '#/admin/milestoneorder/' + order.orderId;


      // if (order.orderId) {
      //   // alert(country.countryId);

      //   $localStorage.order = order;
      //   //


      // }
      // alert("near55");
      // console.log(storeService);
      sellerService.changeorderstatus($scope.orderId, changeId).then(function(response) {
        // alert("reached1");
        loadingService.isLoadin = false;
        // $scope.noPages=Math.ceil(response.totalRecords/$scope.orderPageVo.rowPerPage);
        console.log(response);
        if (response.errorCode === 0) {
          $scope.ordersbystoreId = response.object;
          console.log($scope.ordersbystoreId);
        } else if (response.errorCode === 45 || response.errorCode === 34) {
          $localStorage.token = 0;
          $window.location.href = '#/login';


        };
      }, function() {
        loadingService.isLoadin = false;
      });
    };

    // if ($state.current.name === 'admin.milestoneorder') {
    //   if (!$localStorage.token || $localStorage.token === 0) {
    //       $window.location.href = '#/login';
    //     }


    //      $scope.order = {};
    //       // $scope.customer.password ="password123";
    //       // $scope. category.imageName ="nothing";


    //       // if ($stateParams.orderId !== '0') {
    //         $scope.order = $localStorage.order;
    //         console.log( $scope.order);


    //       // }




    // // alert("haaaaaaaai");





    //   }

    // $scope.ordermanage = function(order) {
    //     loadingService.isLoadin=true;
    //    console.log(order);
    //    $window.location.href = '#/seller/vieworder/' + order.orderId;


    //    if (order.orderId) {
    //      // alert(country.countryId);

    //      $localStorage.order = order;
    //      //


    //    }
    //  };
    $scope.vieworderdetails = function(order) {
      // alert("oh");
      loadingService.isLoadin = true;
      console.log(order);
      $window.location.href = '#/admin/vieworder/' + order.orderId;


      if (order.orderId) {
        // alert(country.countryId);

        $localStorage.order = order;
        //


      }
    };

    $scope.vieworderdetailsbyseller = function(order) {
      // alert("oh");
      loadingService.isLoadin = true;
      console.log(order);
      $window.location.href = '#/admin/vieworder/' + order.orderId;


      if (order.orderId) {
        // alert(country.countryId);

        $localStorage.order = order;
        //


      }
    };
    $scope.viewreturnorderdetailsbyseller = function(ret) {
      // alert("haaaaaaaai");
      // alert("oh");
      loadingService.isLoadin = true;
      console.log(ret);
      var ordId = ret.orderId;
      // console.log();
      var listid = ret.orderListId;
      console.log(listid);
      var customerId = ret.customerId;
      console.log(customerId);
      $localStorage.customerId = customerId;


      $scope.getallorderdetails = function(listid) {
      //  alert("hhhiisd")
        console.log(listid);
        $scope.order = {};
        $scope.orderdetaPageVo =


          {
            "orderId": ordId,
            "orderDetailsId": listid,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "sellerId": 0,
            "currentIndex": 0,
            "rowPerPage": 20
          }
        console.log($scope.orderdetaPageVo);
        // $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        loadingService.isLoadin = true;

        sellerService.getallorderdetails($scope.orderdetaPageVo).then(function(response) {
          loadingService.isLoadin = false;

          // $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.order = response.object;
            console.log($scope.order);
            // $scope.orderdetails=
            $localStorage.order = $scope.order[0];
            console.log($localStorage.order);

            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            // console.log($scope.orderdetails);
            var orderId = $scope.order[0].orderId;
            console.log(orderId);
            $scope.nodata = 0;
            $window.location.href = '#/admin/viewreturn/' + orderId;


          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            loadingService.isLoadin = false;
            $scope.nodata = 1;




          } else {
            // alert("helllo");
           // alert(response.errorMessage);
          }
        });
      }
      $scope.getallorderdetails(listid);


      // $scope.getreturnorderdata(listid);

      // $window.location.href = '#/seller/vieworder' + ret.orderId;



      // if (ret.orderId) {
      //   // alert(country.countryId);

      //   $localStorage.retdetails = retdetails;
      //   //


      // }
    };

    $scope.getreturnorderdata = function() {

      $scope.returnorderdataPageVo =

        {
          "rowPerPage": 25,
          "currentIndex": 0,
          "customerId": 0,
          "fromDate": 0,
          "toDate": 0,
          "orderId": 0,
          "status": [1, 2, 3, 4, 5, 6],
          "storeId": $localStorage.currentloginstoreId,
          "deliveryDate": "0",
          "serviceOrProduct": "",
          "deliveryBoyId": 0,
          "orderListId": listid
        }


      // $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
      loadingService.isLoadin = true;

      sellerService.getallorders($scope.returnorderdataPageVo).then(function(response) {
        loadingService.isLoadin = false;

        // $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
        console.log(response);

        //  if (response.errorCode === 0) {
        //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
        //       $scope.stores = response.object.Store;
        // console.log($scope.stores );

        if (response.errorCode === 0) {
          // $scope.shopList = $scope.shopList.concat(response.object.Product);

          $scope.returnorderdetails = response.object;
          console.log($scope.returnorderdetails);
          // $scope.orderdetails = response.object;
          // [0].orderDetailsVo;
          // console.log($scope.orderdetails);
          $scope.nodata = 0;


        } else if (response.errorCode === 45 || response.errorCode === 34) {
          $localStorage.token = 0;
          $window.location.href = '#/login';


        } else if (response.errorCode === -3) {
          loadingService.isLoadin = false;
          $scope.nodata = 1;




        } else {
          // alert("helllo");
         // alert(response.errorMessage);
        }
      });
    }
    if ($state.current.name === 'seller.vieworder') {
      // alert("oh2");
      if ($localStorage.roleId !== 50) {
        $window.location.href = '#/login'

      }


      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';

      }

      $scope.order = {};
      $scope.order = $localStorage.order;
      $scope.orderdetails = $scope.order.orderDetailsVo;
      console.log($scope.orderdetails);
      // if (order.orderId) {
      //   // alert(country.countryId);

      //   $scope.order= $localStorage.order;
      //   //


      // }
      console.log($scope.order);



      // alert(33333333);

      $scope.customerPageVo = {
        "customerId": 0,
        "cityId": 0,
        "countryId": 0,
        "userId": $scope.order.customerId,
        "fromDate": 0,
        "toDate": 0,
        "currentIndex": 0,
        "rowPerPage": "100",
        "status": 2,
        "customerName": "",
        "phoneNo": "",
        "emailId": ""

      }

      $scope.getallcustomers = function() {

        // alert("haai");
        loadingService.isLoadin = true;

        inputService.getallcustomers($scope.customerPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.customer = response.object[0];
            console.log($scope.customer);



          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
           // alert(response.errorMessage);
          }
        }, function() {
          //alert('Oops! somthing went wrong');
        });
      };
      $scope.getallcustomers();



    }
    $scope.editorder = function(order) {
      loadingService.isLoadin = true;
      console.log(order);
      $window.location.href = '#/admin/editorder/' + order.orderId;


      if (order.orderId) {
        // alert(country.countryId);

        $localStorage.order = order;
        //


      }
    };

    if ($state.current.name === 'admin.editorder') {
      if ($localStorage.roleId !== 7) {
        $window.location.href = '#/login'

      }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';

      }

      $scope.order = {};
      $scope.order = $localStorage.order;
      $scope.orderdetails = $scope.order.orderDetailsVo;
      console.log($scope.orderdetails);
      // if (order.orderId) {
      //   // alert(country.countryId);

      //   $scope.order= $localStorage.order;
      //   //


      // }
      console.log($scope.order);



      // alert(33333333);
      $scope.getallcustomers = function() {
        $scope.customerPageVo = {
          "customerId": $scope.order.customerId,
          "cityId": 0,
          "countryId": 0,
          "userId": 0,
          "orderBy": 0,
          "fromDate": 0,
          "toDate": 0,
          "currentIndex": 0,
          "rowPerPage": 100,
          "status": 2
        }
        console.log($scope.customerPageVo);
        loadingService.isLoadin = true;

        inputService.getallcustomers($scope.customerPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.customer = response.object;
            console.log($scope.customer);



          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
           // alert(response.errorMessage);
          }
        }, function() {
         // alert('Oops! somthing went wrong');
        });
      };
      $scope.getallcustomers();

    }
    $scope.AllocateDeliverBoy = function(order) {
      // alert("haaai");
      loadingService.isLoadin = true;
      console.log(order);
      $window.location.href = '#/seller/allocateDeliveBoy/' + order.orderId;

      $localStorage.order = order;
      loadingService.isLoadin = false;
      //



    };

    if ($state.current.name === 'seller.allocateDeliveBoy') {
      // alert("hoo");
      if ($localStorage.roleId !== 50) {
        $window.location.href = '#/login'

      }
      $scope.order = {};


      $scope.order = $localStorage.order;

      $scope.getnearestdeliveryboys = function() {

        // $scope.deliveryboyPageVo = {"customerId":$scope.order.customerId,
        //      "cityId":0,
        //      "countryId":0,
        //      "userId":0,
        //      "orderBy":0,
        //      "fromDate":0,
        //      "toDate":0,
        //      "currentIndex":0,
        //      "rowPerPage":100,
        //      "status":2}
        //      console.log( $scope.deliveryboyPageVo);

        loadingService.isLoadin = true;

        inputService.getallnearestdeliveryboys().then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.nearestdeliveryboys = response.object;
            console.log($scope.nearestdeliveryboys);

            $scope.nearest = 0;

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -7) {
            $scope.nearest = 1;

          } else {
          //  alert(response.errorMessage);
          }
        }, function() {
        //  alert('Oops! somthing went wrong');
        });
      };
      $scope.getnearestdeliveryboys();




      $scope.assignworktodeliveryboy = function(deliveryboy, order) {
        console.log(deliveryboy, order);
        var orderid = order.orderId;
        $scope.deliveryboyPageVo = {
          "deliveryBoyId": deliveryboy.deliveryBoyId,
          "orderId": orderid,
          "deliverStatus": 1
        }

        console.log($scope.deliveryboyPageVo);


        console.log(deliveryboy, orderid);

        var orderId = orderid;
        console.log(orderId);
        var deliveryboyId = deliveryboy.deliveryBoyId;
        console.log(deliveryboyId);
        // console.log(orderId);
        //  console.log(orderstatus);


        loadingService.isLoadin = true;

        inputService.assignworktodeliveryboy($scope.deliveryboyPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);



          if (response.errorCode === 0) {

            toaster.pop('success', "Successfull..!", "Assigned the DeliveryBoy!");

            $scope.Confirmedorder(order);
            $window.location.href = '#/seller/neworder';







          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            toaster.pop('info', "Oops..!", response.errorMessage);
            // alert(response.errorMessage);
          }
        }, function() {
        //  alert('Oops! somthing went wrong');
        });
      };

      // sedfhjklhg

      $scope.assignworktodeliveryboyofreturn = function(deliveryboy, order) {
        console.log(deliveryboy, order);
        var orderid = order.orderId;
        $scope.deliveryboyPageVo = {
          "deliveryBoyId": deliveryboy.deliveryBoyId,
          "orderId": orderid,
          "deliverStatus": 4
        }

        console.log($scope.deliveryboyPageVo);


        console.log(deliveryboy, orderid);

        var orderId = orderid;
        console.log(orderId);
        var deliveryboyId = deliveryboy.deliveryBoyId;
        console.log(deliveryboyId);
        // console.log(orderId);
        //  console.log(orderstatus);


        loadingService.isLoadin = true;

        inputService.assignworktodeliveryboy($scope.deliveryboyPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);



          if (response.errorCode === 0) {

            toaster.pop('success', "Successfull..!", "Assigned the DeliveryBoy!");

            $scope.confirmedreturnorder(order);
            $window.location.href = '#/seller/returns';







          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            toaster.pop('info', "Oops..!", response.errorMessage);
            // alert(response.errorMessage);
          }
        }, function() {
         // alert('Oops! somthing went wrong');
        });
      };



    }
    // change returns status


    $scope.confirmedreturnorder = function(order) {
      console.log(order);

      var returnItemId = order.returnItemId;
      var orderid = order.orderId;
      var orderDetailsIds = order.orderListId;
      var status = 11;

      console.log(returnItemId);
      console.log(status);
      // $scope.deliveryboyPageVo = {"customerId":$scope.order.customerId,
      //      "cityId":0,
      //      "countryId":0,
      //      "userId":0,
      //      "orderBy":0,
      //      "fromDate":0,
      //      "toDate":0,
      //      "currentIndex":0,
      //      "rowPerPage":100,
      //      "status":2}
      //      console.log( $scope.deliveryboyPageVo);

      loadingService.isLoadin = true;

      inputService.confirmingreturnitem(returnItemId).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);



        if (response.errorCode === 0) {
          toaster.pop('success', "Successfull..!", "Confirmed the Return Request!");
          //        $scope.getalldeliveredorders(1);
          // $scope.getallrejectedorders(1);
          // $scope.getallconfirmedorders(1);
          $scope.getallproductreturns(1);

          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          // $scope.nearestdeliveryboys = response.object;
          // console.log($scope.nearestdeliveryboys);



        } else {
          // alert(response.errorMessage);
        }
      }, function() {
        //alert('Oops! somthing went wrong');
      });
    };

    $scope.returedtheorder = function(order) {
      console.log(order);

      var returnItemId = order.returnItemId;
      var orderid = order.orderId;
      var orderDetailsIds = order.orderListId;
      // var status =11;

      console.log(returnItemId);
      // console.log(status);


      loadingService.isLoadin = true;

      inputService.returnedreturnitem(returnItemId).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);



        if (response.errorCode === 0) {
          toaster.pop('success', "Successfull..!", "Successfully Completed the Return !");
          //        $scope.getalldeliveredorders(1);
          // $scope.getallrejectedorders(1);
          // $scope.getallconfirmedorders(1);
          $scope.getallconfirmedreturns(1);

          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          // $scope.nearestdeliveryboys = response.object;
          // console.log($scope.nearestdeliveryboys);



        } else {
          // alert(response.errorMessage);
        }
      }, function() {
        //alert('Oops! somthing went wrong');
      });
    };

    // REJECT RETURN ORDER

    $scope.RejectReturn = function(order) {
      console.log(order);

      var returnItemId = order.returnItemId;
      var orderid = order.orderId;
      var orderDetailsIds = order.orderListId;
      // var status =11;

      console.log(returnItemId);
      console.log(status);
      // $scope.deliveryboyPageVo = {"customerId":$scope.order.customerId,
      //      "cityId":0,
      //      "countryId":0,
      //      "userId":0,
      //      "orderBy":0,
      //      "fromDate":0,
      //      "toDate":0,
      //      "currentIndex":0,
      //      "rowPerPage":100,
      //      "status":2}
      //      console.log( $scope.deliveryboyPageVo);

      loadingService.isLoadin = true;

      inputService.rejectingreturnitem(orderid, orderDetailsIds).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);



        if (response.errorCode === 0) {
          toaster.pop('success', "Successfull..!", "Successfully Rejected the Return Request!");
          //        $scope.getalldeliveredorders(1);
          // $scope.getallrejectedorders(1);
          // $scope.getallconfirmedorders(1);
          $scope.getallproductreturns(1);

          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          // $scope.nearestdeliveryboys = response.object;
          // console.log($scope.nearestdeliveryboys);



        } else {
          // alert(response.errorMessage);
        }
      }, function() {
       // alert('Oops! somthing went wrong');
      });
    };


    $scope.AllocateDeliverBoyforreturn = function(order) {
      // alert("haaai");
      loadingService.isLoadin = true;
      console.log(order);
      $window.location.href = '#/seller/allocateDeliveBoy/' + order.returnItemId;

      $localStorage.order = order;
      loadingService.isLoadin = false;
      //



    };

    $scope.Confirmedorder = function(order) {

      var orderId = order.orderId;
      var orderstatus = 2;
      console.log(orderId);
      console.log(orderstatus);
      // $scope.deliveryboyPageVo = {"customerId":$scope.order.customerId,
      //      "cityId":0,
      //      "countryId":0,
      //      "userId":0,
      //      "orderBy":0,
      //      "fromDate":0,
      //      "toDate":0,
      //      "currentIndex":0,
      //      "rowPerPage":100,
      //      "status":2}
      //      console.log( $scope.deliveryboyPageVo);

      loadingService.isLoadin = true;

      inputService.changeorderstatus(orderId, orderstatus).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);



        if (response.errorCode === 0) {
          $scope.getallorders(1);
          toaster.pop('success', "Successfull..!", "Confirmed the Order!");
          //        $scope.getalldeliveredorders(1);
          // $scope.getallrejectedorders(1);
          // $scope.getallconfirmedorders(1);


          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          // $scope.nearestdeliveryboys = response.object;
          // console.log($scope.nearestdeliveryboys);



        } else if (response.errorCode === 45 || response.errorCode === 34) {
          $localStorage.token = 0;
          $window.location.href = '#/login';


        } else {
          // alert(response.errorMessage);
        }
      }, function() {
       // alert('Oops! somthing went wrong');
      });
    };
    $scope.ChangetoDelivered = function(order) {

      var orderId = order.orderId;
      var orderstatus = 4;
      console.log(orderId);
      console.log(orderstatus);
      // $scope.deliveryboyPageVo = {"customerId":$scope.order.customerId,
      //      "cityId":0,
      //      "countryId":0,
      //      "userId":0,
      //      "orderBy":0,
      //      "fromDate":0,
      //      "toDate":0,
      //      "currentIndex":0,
      //      "rowPerPage":100,
      //      "status":2}
      //      console.log( $scope.deliveryboyPageVo);

      loadingService.isLoadin = true;

      inputService.changeorderstatus(orderId, orderstatus).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);



        if (response.errorCode === 0) {
          $scope.getallpickedorders(1);
          toaster.pop('success', "Successfull..!", "Delivered the Order!");
          //        $scope.getalldeliveredorders(1);
          // $scope.getallrejectedorders(1);
          // $scope.getallconfirmedorders(1);


          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          // $scope.nearestdeliveryboys = response.object;
          // console.log($scope.nearestdeliveryboys);



        } else if (response.errorCode === 45 || response.errorCode === 34) {
          $localStorage.token = 0;
          $window.location.href = '#/login';


        } else {
          // alert(response.errorMessage);
        }
      }, function() {
       // alert('Oops! somthing went wrong');
      });
    };

    $scope.ChangetoShipping = function(order) {

      var orderId = order.orderId;
      var orderstatus = 6;
      console.log(orderId);
      console.log(orderstatus);
      // $scope.deliveryboyPageVo = {"customerId":$scope.order.customerId,
      //      "cityId":0,
      //      "countryId":0,
      //      "userId":0,
      //      "orderBy":0,
      //      "fromDate":0,
      //      "toDate":0,
      //      "currentIndex":0,
      //      "rowPerPage":100,
      //      "status":2}
      //      console.log( $scope.deliveryboyPageVo);

      loadingService.isLoadin = true;

      inputService.changeorderstatus(orderId, orderstatus).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);



        if (response.errorCode === 0) {
          $scope.getallconfirmedorders(1);
          toaster.pop('success', "Successfull..!", "Successfully Shipped the Order!");
          //        $scope.getalldeliveredorders(1);
          // $scope.getallrejectedorders(1);
          // $scope.getallconfirmedorders(1);


          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          // $scope.nearestdeliveryboys = response.object;
          // console.log($scope.nearestdeliveryboys);



        } else if (response.errorCode === 45 || response.errorCode === 34) {
          $localStorage.token = 0;
          $window.location.href = '#/login';


        } else {
          // alert(response.errorMessage);
        }
      }, function() {
       // alert('Oops! somthing went wrong');
      });
    };

    $scope.rejectOrder = function(order) {

      var orderId = order.orderId;
      var orderstatus = 3;
      console.log(orderId);
      console.log(orderstatus);
      // $scope.deliveryboyPageVo = {"customerId":$scope.order.customerId,
      //      "cityId":0,
      //      "countryId":0,
      //      "userId":0,
      //      "orderBy":0,
      //      "fromDate":0,
      //      "toDate":0,
      //      "currentIndex":0,
      //      "rowPerPage":100,
      //      "status":2}
      //      console.log( $scope.deliveryboyPageVo);

      loadingService.isLoadin = true;

      inputService.confirmingorder(orderId, orderstatus).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);



        if (response.errorCode === 0) {
          toaster.pop('success', "Successfull..!", "Rejected the Order!");
          //        $scope.getalldeliveredorders(1);
          // $scope.getallrejectedorders(1);
          // $scope.getallconfirmedorders(1);
          $scope.getallorders(1);

          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          // $scope.nearestdeliveryboys = response.object;
          // console.log($scope.nearestdeliveryboys);



        } else {
          // alert(response.errorMessage);
        }
      }, function() {
      //  alert('Oops! somthing went wrong');
      });
    };
    // $scope.getnearestdeliveryboys(); 




    // vieworderdetails

    $scope.vieworderdetails = function(order) {
      loadingService.isLoadin = true;
      console.log(order);
      $window.location.href = '#/admin/vieworder/' + order.orderId;


      if (order.orderId) {
        // alert(country.countryId);

        $localStorage.order = order;
        //


      }
    };
    if ($state.current.name === 'admin.vieworder') {
      // alert("haaaaaaaai");
      // if( $localStorage.roleId !== 7){
      //   $window.location.href ='#/login'

      // }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }
      $scope.order = {};
      $scope.order = $localStorage.order;
      console.log($scope.order);
      $scope.orderdetails = $scope.order.orderDetailsVo;
      $scope.shippingDetails = $scope.order.shippingDetails;
      console.log($scope.shippingDetails);


      console.log($scope.orderdetails);
      console.log($scope.order);
      loadingService.isLoadin = false;
      var customerId = $scope.order.customerId;


      $scope.getcustomer = function(customerId) {
        // alert("success1");
        $scope.customerPageVo = {
          "customerId": 0,
          "cityId": 0,
          "countryId": 0,
          "userId": customerId,
          "fromDate": 0,
          "toDate": 0,
          "currentIndex": 0,
          "rowPerPage": "75",
          "status": 2,
          "customerName": "",
          "phoneNo": "",
          "emailId": ""

        }
        console.log($scope.customerPageVo);

        loadingService.isLoadin = true;


        inputService.getallcustomers($scope.customerPageVo).then(function(response) {
          loadingService.isLoadin = false;

          console.log(response);



          if (response.errorCode === 0) {

            $scope.customerinfo = response.object[0];
            console.log($scope.customerinfo);



          } else {
           // alert(response.errorMessage);
          }
        }, function() {
       //   alert('Oops! somthing went wrong');
        });
      };
      $scope.getcustomer(customerId);


    }
    // VIEW RETURN ORDER
    if ($state.current.name === 'admin.viewreturn') {
    //  alert("new")

      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }
      var customerId = $localStorage.customerId;
      console.log(customerId);
      $scope.order = {};
      $scope.order = $localStorage.order;
      console.log($scope.order);
      $scope.orderdetails = $scope.order.orderDetailsVo;
      $scope.shippingDetails = $scope.order.shippingDetails;
      console.log($scope.shippingDetails);


      console.log($scope.orderdetails);
      console.log($scope.order);
      loadingService.isLoadin = false;

      $scope.getcustomer = function() {
        // alert("success1");
        $scope.customerPageVo = {
          "customerId": 0,
          "cityId": 0,
          "countryId": 0,
          "userId": customerId,
          "fromDate": 0,
          "toDate": 0,
          "currentIndex": 0,
          "rowPerPage": "75",
          "status": 2,
          "customerName": "",
          "phoneNo": "",
          "emailId": ""

        }
        console.log($scope.customerPageVo);

        loadingService.isLoadin = true;


        inputService.getallcustomers($scope.customerPageVo).then(function(response) {
          loadingService.isLoadin = false;

          console.log(response);



          if (response.errorCode === 0) {

            $scope.customerifo = response.object[0];
            console.log($scope.customerifo);



          } else {
            //alert(response.errorMessage);
          }
        }, function() {
         // alert('Oops! somthing went wrong');
        });
      };
      $scope.getcustomer();


    }

    // VIEW RETURN ORDER
    if ($state.current.name === 'admin.viewreturnorder') {
      // alert("haaaaaaaai");
      // if( $localStorage.roleId !== 7){
      //   $window.location.href ='#/login'

      // }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }
      $scope.order = {};
      $scope.order = $localStorage.order;
      console.log($scope.order);
      $scope.orderdetails = $scope.order.orderDetailsVo;
      $scope.shippingDetails = $scope.order.shippingDetails;
      console.log($scope.shippingDetails);


      console.log($scope.orderdetails);
      console.log($scope.order);
      loadingService.isLoadin = false;
      var customerId = $scope.order.customerId;


      // $scope. country.countryFlag ='1';
      // if ($stateParams.cityId !== '0') {
      //   $scope.city = $localStorage.city;
      // }
      // $scope.getcustomer(customerId);
      //  $scope.getallcustomers(customerId);
      // $scope.getallconfirmedorders(1);
      $scope.getcustomer = function(customerId) {
        // alert("success1");
        $scope.customerPageVo = {
          "customerId": 0,
          "cityId": 0,
          "countryId": 0,
          "userId": customerId,
          "fromDate": 0,
          "toDate": 0,
          "currentIndex": 0,
          "rowPerPage": "75",
          "status": 2,
          "customerName": "",
          "phoneNo": "",
          "emailId": ""

        }
        console.log($scope.customerPageVo);
        // alert("haai");
        loadingService.isLoadin = true;
        // $scope.customerPageVo.currentIndex = (pageno - 1) * $scope.customerPageVo.rowPerPage;

        inputService.getallcustomers($scope.customerPageVo).then(function(response) {
          loadingService.isLoadin = false;
          // $scope.noPages = Math.ceil(response.totalRecords / $scope.customerPageVo.rowPerPage);
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.customerifo = response.object[0];
            console.log($scope.customerifo);



          } else {
            //alert(response.errorMessage);
          }
        }, function() {
          //alert('Oops! somthing went wrong');
        });
      };
      $scope.getcustomer(customerId);


    }


    $scope.pagereload = function() {
      window.location.reload();

    }
    $scope.orderSearchwithOrderId = function(orderid) {
      console.log(orderid);


      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;

      $scope.getallorders = function(pageno) {
        $scope.orderPageVo =

          {
            "rowPerPage": 25,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": orderid,
            "status": [1],
            "storeId": 0,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }
        console.log($scope.orderPageVo);
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        // loadingService.isLoadin=true;
        $scope.loading = 1;

        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          // loadingService.isLoadin=false;
          $scope.loading = 0;

          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.neworders = response.object;
            console.log($scope.neworders);
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            // console.log($scope.orderdetails);
            $scope.nodata = 0;


          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            $scope.loading = 0;
            // loadingService.isLoadin=false;
            $scope.neworders = [];
            $scope.nodata = 1;




          } else {
            // alert("helllo");
         //   alert(response.errorMessage);
          }
        });
      }
      $scope.getallorders($scope.pageno);

    }
    // SEARCH Confirmed orders

    $scope.confirmedorderSearchwithOrderId = function(orderid) {
      console.log(orderid);


      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;

      $scope.getallconfirmedorders = function(pageno) {
        $scope.orderPageVo =

          {
            "rowPerPage": 25,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": orderid,
            "status": [2],
            "storeId": 0,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.confirmedorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.confirmedorders);
            $scope.noconfirmeddata = 0;

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            $scope.confirmedorders = [];
            // loadingService.isLoadin=false;
            $scope.loading = 0;
            $scope.noconfirmeddata = 1;




          } else {
           // alert(response.errorMessage);
          }
        });
      }
      $scope.getallconfirmedorders($scope.pageno);

    }

    // SEARCH REJECTED ORDERS

    $scope.rejectedorderSearchwithOrderId = function(orderid) {
      console.log(orderid);


      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;

      $scope.getallrejectedorders = function(pageno) {
        $scope.orderPageVo =

          {
            "rowPerPage": 25,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": orderid,
            "status": [3],
            "storeId": 0,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.rejectedorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.rejectedorders);
            $scope.norejecteddata = 0;
          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            // loadingService.isLoadin=false;
            $scope.rejectedorders = [];
            $scope.loading = 0;
            $scope.norejecteddata = 1;
          } else {
           // alert(response.errorMessage);
          }
        });
      }
      $scope.getallrejectedorders($scope.pageno);

    }

    // SEARCH PICKED ORDERS

    $scope.pickedorderSearchwithOrderId = function(orderid) {
      console.log(orderid);


      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;

      $scope.getallpickedorders = function(pageno) {
        $scope.pickedorderPageVo =

          {
            "rowPerPage": 25,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": orderid,
            "status": [6],
            "storeId": 0,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.pickedorderPageVo.currentIndex = (pageno - 1) * $scope.pickedorderPageVo.rowPerPage;
        sellerService.getallorders($scope.pickedorderPageVo).then(function(response) {
          $scope.noPages = Math.ceil(response.totalRecords / $scope.pickedorderPageVo.rowPerPage);
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.pickedorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.pickedorders);
            $scope.nopickeddata = 0;
          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            $scope.pickedorders = [];
            // loadingService.isLoadin=false;
            $scope.loading = 0;
            $scope.nopickeddata = 1;
            console.log($scope.nopickeddata);
          } else {
           // alert(response.errorMessage);
          }
        });
      }
      $scope.getallpickedorders($scope.pageno);

    }

    // SEARCH DELIVERED ORDERS
    $scope.deliveredSearchwithOrderId = function(orderid) {
      console.log(orderid);


      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;

      $scope.getalldeliveredorders = function(pageno) {
        $scope.orderPageVo =

          {
            "rowPerPage": 25,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": orderid,
            "status": [4],
            "storeId": 0,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.deliveredorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.deliveredorders);
            $scope.nodelivereddata = 0;
          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            $scope.deliveredorders = [];
            // loadingService.isLoadin=false;
            $scope.loading = 0;
            $scope.nodelivereddata = 1;
            console.log($scope.nodelivereddata);
          } else {
            //alert(response.errorMessage);
          }
        });
      }

      $scope.getalldeliveredorders($scope.pageno);

    }



  });

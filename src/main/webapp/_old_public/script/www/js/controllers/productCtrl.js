angular.module('starter.productCtrl', [])
  .controller('productCtrl', function(FILE_CONSTANT, loadingService, MAIN_CONSTANT, $scope, toaster, $window, Upload, $state, inputService, storeService, $stateParams, $localStorage) {
    //alert($localStorage.token);
if (!$localStorage.token||$localStorage.token===0) {
    $window.location.href ='#/login';
    }
    $window.scrollTo(0, 0);

    $scope.imageUrl = FILE_CONSTANT.filepath;
     $scope.selectedproducts=[];


    // $scope.product = {};


    $scope.upload = function(file) {
      //$scope.isFileuploadStart=true;



      if (file) {
        var featurejson = {
          "id": new Date().getTime(),
          "modeId": 5,
          "isLoadin": true,
          "extraDataNum": 0,
          "isLoadin": true


        };


        $scope.product.featureOrPhotos.push(featurejson);
        var id = featurejson.id;

        $scope.clickimg = true;


        console.log(file);
        Upload.upload({
          url: MAIN_CONSTANT.site_url + "/api/uploadtoaws",
          headers: {
            "Content-Type": 'multipart/*',
            Bearer: $localStorage.token
          },
          /*data: { file: FILE_CONSTANT.site_url + file.name,
            type: 'image/jpeg' }*/
          data: {
            file: file
          }
        }).then(function(resp) {

          console.log(resp);

          //$scope.product.featureOrPhotos[lastPush-1].data= resp.data.object;
          if (resp.data.errorCode === 0) {

            for (var i = 0; i < $scope.product.featureOrPhotos.length; i++) {
              if ($scope.product.featureOrPhotos[i].id == id) {
                console.log(resp);
                $scope.product.featureOrPhotos[i].data = resp.data.object;
                $scope.product.featureOrPhotos[i].isLoadin = false;

              }
            }


            if (!$scope.product.featureOrPhotos) {
              $scope.product.featureOrPhotos = [];
            }
          } else if (resp.data.errorCode === 45 || resp.data.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';

          } else {

          }





        }, function(resp) {
          $scope.progressPercentage = 0;
          for (var i = 0; i < $scope.product.featureOrPhotos.length; i++) {
            if ($scope.product.featureOrPhotos[i].id == id) {
              //splice i
              $scope.product.featureOrPhotos.splice(i, 1);

            }
          }

          console.log('Error status: ' + resp.status);


        }, function(evt) {
          $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
          console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
          // $scope.file = null;
          if ($scope.progressPercentage < 100 && $scope.progressPercentage != 0) {
            $scope.filecmplt = false;
          } else if ($scope.progressPercentage == 100) {
            $scope.filecmplt = true;

          } else {
            $scope.filenotUplaod = true;
          }
        });
      }
    };

    $scope.changeCategory = function(categoryId) {
      console.log(categoryId);


      $scope.getsubcategoryByCategoryId(categoryId)

    };
    $scope.getsubcategoryByCategoryId = function(categoryId) {
      $scope.subcategoriesbycategories = [];
      inputService.getsubcategoriesByCategoryId(categoryId).then(function(response) {
        console.log(response);
        if (response.errorCode === 0) {
          $scope.subcategoriesbycategories = response.object;
          console.log($scope.subcategoriesbycategories);
        }
      });
    };
    /*    $scope.getsubcategoryByCategoryId();*/





    // };




    // }
    //  scope.getsubcategoryByCatid=function(catid){
    //   slkdjfsd(catiod).then(function(res){
    //     scope.subcategories=res.data;
    //   })
    //  }


    //end test

    // if ($state.current.name === 'admin.store') {
    //   if (!$localStorage.token || $localStorage.token === 0) {
    //   $window.location.href = '#/login';
    // }

    //   $scope.storePageVo = { "rowPerPage": 100, "currentIndex": 0, "shopId": 0, "status": [] }


    //   $scope.allstore = function() {
    //     loadingService.isLoadin = true;

    //     storeService.getallstores($scope.storePageVo).then(function(response) {
    //       loadingService.isLoadin = false;
    //       console.log(response);



    //       if (response.errorCode === 0) {
    //         // $scope.shopList = $scope.shopList.concat(response.object.Product);
    //         $scope.stores = response.object.Store;
    //         console.log($scope.stores);



    //       } else {
    //         alert(response.errorMessage);
    //       }
    //     });
    //   }
    //   $scope.allstore();




    // }



    //   //view product
    

    if ($state.current.name === 'admin.viewproduct') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
      $scope.product = {};
      // $scope.product.imageName = "nothing";
      if ($stateParams.productId !== '0') {

        $scope.product = $localStorage.product;
        $scope.productimages = $scope.product.featureOrPhotos;
        console.log($scope.productimages);

      }
      loadingService.isLoadin = false;

    }






    // add product

    if ($state.current.name === 'admin.addproduct') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }

    $scope.deletefield = function(name) {
        if ($scope.product.productAttributeVos.length <= 1) {
          $scope.product.productAttributeVos.length = [];

        } else {
          for (var i = 0; i < $scope.product.productAttributeVos.length; i++) {
            if ($scope.product.productAttributeVos[i] == name) {
              $scope.product.productAttributeVos.splice(i, 1);

            }
          }
        }
        //$scope.product.featureOrPhotos[i].data

      }
     $scope.getattributesforaddproduct = function(atbid) {
   loadingService.isLoadin=true;
      inputService.getattributesforaddproduct(atbid).then(function(response) {
         loadingService.isLoadin= false;
        console.log(response);
        if (response.errorCode === 0) {
          $scope.attributesforaddproduct = response.object;
          console.log($scope.attributesforaddproduct);
        }else if (response.errorCode === 45) {
           loadingService.isLoadin=false;
                        //  console.log( );
                 $localStorage.token = 0;
           $window.location.href = '#/login';


        };
      });
    };
 $scope.getattributesforaddproduct();

      $scope.getallmanufacture = function() {
         loadingService.isLoadin=true;
            $scope.manufacturePageVo =  {
        "manufacturerId":0,
        "manufacturerName":"",

        "status":1,
        "currentIndex":0,
        "rowPerPage":100
}

        inputService.getallmanufactures($scope.manufacturePageVo).then(function(response) {
           loadingService.isLoadin=false;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.manufactures = response.object;

          } else {
           // alert(response.errorMessage);
          }
        });
      }
      $scope.getallmanufacture();

 $scope.addtheattribute = function() {
              // alert("222")

   // if(product.attributVo){
   //      product.attributVo=[];
   //                          }
 if(!$scope.product.productAttributeVos){
  $scope.product.productAttributeVos=[];

 }
  var attributejson = {
    "proudctId": 0,
     "attributeId": 0,
     "value": "",
     "temp":new Date().getTime()
          
        };

        console.log($scope.product.productAttributeVos);

   $scope.product.productAttributeVos.push(attributejson);
     console.log($scope.product.productAttributeVos);


  


  
    };




    $scope.product = {};
     $scope.product.connectedProductsVo = [];
      $scope.selectedcount= $scope.selectedproducts.length;
      console.log( $scope.selectedcount);
          $scope.pId = $stateParams.productId;
          console.log( $scope.pId);
          // alert($scope.pId);

           $scope.chooseproduct=function(product,modeId){

            // alert("ent");
            console.log(product,modeId);
            $scope.pId=product.productId;
              $scope.productName=product.productName;
               $scope.productNameAr=product.productNameAr;
            console.log($scope.pId);
            // ############

            
      //$scope.isFileuploadStart=true;

 // $scope.uniqueId = new Date().getTime();

      // if (file) {
        var featurejson = {
          // "id": new Date().getTime(),
          "modeId": modeId,
         "uniqueId": new Date().getTime(),

         
          "isLoadin": true,
          "extraDataNum": 0,
          "connectedProductsId": $scope.pId,
            "productName": $scope.productName,
             "productNameAr": $scope.productNameAr,
          "isLoadin": true


        };
        console.log(featurejson);


        $scope.product.connectedProductsVo.push(featurejson);
        var id = featurejson.id;

        $scope.clickimg = true;
console.log( $scope.product.connectedProductsVo);
          $scope.selectedproducts=$scope.product.connectedProductsVo;
         $scope.selectedcount= $scope.selectedproducts.length;
          console.log( $scope.selectedproducts);
          console.log( $scope.selectedcount);

      
           }

           $scope.getalltax = function() {
        loadingService.isLoadin = true;
        $scope.taxPageVo = {"countryId":0,"cityId":0,"taxId":0,"currentIndex":0,"rowPerPage":100,"stat":1}
        storeService.getactivetaxes($scope.taxPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.taxes = response.object;
            console.log($scope.taxes);



          }else if(response.errorCode===-3){
             loadingService.isLoadin = false;



          } else {
          //  alert(response.errorMessage);
          }
        });
      }

      $scope.getalltax();
      
     

      $scope.getproductforrelated = function(catid,subcatid) {
        console.log(catid,subcatid);

        loadingService.isLoadin = true;
         $scope.productPageVo=
     {"rowPerPage": "15",
       "currentIndex": 0,
       "productName": "",
       "productId": "0",
       "subCategoryId": subcatid,"countryId":0,"cityId":0,"userId":0,
       "categoryId": catid,
       "sellerId": "0",
       "stat": "1",
       "status": []}
        // $scope.productPageVo.currentIndex = (pageno - 1) * $scope.productPageVo.rowPerPage;



        inputService.getproducts($scope.productPageVo).then(function(response) {
          loadingService.isLoadin = false;

          // $scope.noPages = Math.ceil(response.object.TOTALCOUNTS / $scope.productPageVo.rowPerPage);
          loadingService.isLoadin = false;
          console.log(response);
          if (response.errorCode === 0) {
            $scope.productsforrelated = response.object.Product;

            console.log($scope.productsforrelated);
            console.log($scope.productimages);
            $scope.noproductsforrelated=0;
          }else if(response.errorCode=== -3){
 $scope.noproductsforrelated=1;
  $scope.productsforrelated =[];

          }
          else if (response.errorCode===45||response.errorCode===34) {
             $localStorage.token = 0;
           $window.location.href = '#/login';
          
        }
        }, function() {
         // alert("Oops! error ode");
          loadingService.isLoadin = false
        });
      };
      $scope.getproductforrelated();

      // $scope.allstore = function() {
      //   loadingService.isLoadin = true;
      //   $scope.storePageVo = { "rowPerPage": 100, "currentIndex": 0, "shopId": 0, "status": [] }

      //   storeService.getallstores($scope.storePageVo).then(function(response) {
      //     loadingService.isLoadin = false;
      //     console.log(response);



      //     if (response.errorCode === 0) {
      //       // $scope.shopList = $scope.shopList.concat(response.object.Product);
      //       $scope.stores = response.object.Store;
      //       console.log($scope.stores);



      //     } else {
      //       alert(response.errorMessage);
      //     }
      //   });
      // }
      // $scope.allstore();


      $scope.getactivecategory = function(maincat) {
        loadingService.isLoadin = true;
        inputService.getactivecategories(maincat).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);
          if (response.errorCode === 0) {
            $scope.categories = response.object;
            console.log($scope.categories);
          }
        });
      };
      $scope.getactivecategory();


      // $scope.product = {};

      $scope.deleteimage = function(name) {
        if ($scope.product.featureOrPhotos.length <= 1) {
          $scope.product.featureOrPhotos.length = [];

        } else {
          for (var i = 0; i < $scope.product.featureOrPhotos.length; i++) {
            if ($scope.product.featureOrPhotos[i].data == name) {
              $scope.product.featureOrPhotos.splice(i, 1);

            }
          }
        }
        //$scope.product.featureOrPhotos[i].data

      }

      $scope.product.featureOrPhotos = []


      // $scope.product.imageName = "nothing";
      if ($stateParams.productId !== '0') {
        
$scope.product = $localStorage.product;
$scope.prid=1;

         $scope.getsubcategoryByCategoryId($scope.product.categoryId)
        console.log($scope.product);
        $scope.product = $localStorage.product;
        // console.log($scope.getsubcategoryByCategoryId);
      }else{
        $scope.prid=0;
      }

      $scope.selectcategory = function(categoryId) {
        console.log(categoryId);
        $scope.product = { "categoryId": categoryId.categoryId };
      }
// ######
$scope.getrelations = function(relation) {
        loadingService.isLoadin = true;

      //   $scope.relations = inputService.getrelations(relation)
      //   // .then(function(response) {
      //     loadingService.isLoadin = false;
      //     console.log($scope.relations );
      //     // if (response.errorCode === 0) {
      //     //   $scope.relations = response.object;
      //     //   console.log($scope.relations);
      //     // }
      //   // });

      $scope.relations = [
      {"modeId": "8", "name": "COLOUR","nameAr": 'اللون '},
     {"modeId": "9", "name": "MATERIAL","nameAr": "مادة"},
      {"modeId": "10", "name": "TYPE","nameAr": "نوع"},
      {"modeId": "11", "name": "CAPACITY","nameAr":"سعة" },
      {"modeId": "12", "name": "SIZE","nameAr":"بحجم" },
      {"modeId": "13", "name": "LENGTH","nameAr":"الطول" },
      {"modeId": "14", "name": "WIDTH","nameAr":"عرض" },
      {"modeId": "15", "name": "HEIGHT","nameAr":"ارتفاع" },
       {"modeId": "16", "name": "WEIGHT","nameAr": "وزن"},
       ];
        console.log($scope.relations);
      };
      

      $scope.getrelations();

      // #######
      //  $scope.getrelations = function(relation) {
      //  $scope.relations = inputService.getrelations(relation)
      //  console.log( $scope.relations);
      //   // loadingService.isLoadin = true;
      //   // inputService.getrelations(relation).then(function(response) {
      //   //   loadingService.isLoadin = false;
      //   //   console.log(response);
      //   //   if (response.errorCode === 0) {
      //   //     $scope.relations = response.object;
      //   //     console.log($scope.relations);
      //   //   }
      //   // });
      // };
      // $scope.getrelations();


      //loadingService.isLoadin = true;
// $scope.product.sellerId = $localStorage.sellerId;
// console.log($scope.product.sellerId);

//   $scope.product.minimumPurchaseLimit= parseFloat($scope.product.minimumPurchaseLimit);
//  console.log( $scope.product.minimumPurchaseLimit);
 
//  $scope.product.minimumPurchaseLimitAr= parseFloat($scope.product.minimumPurchaseLimitAr);
//  console.log( $scope.product.minimumPurchaseLimitAr);

$scope.product.sellerId = $localStorage.sellerId;
 console.log($scope.product.sellerId);

      $scope.addProduct = function(getAddProductDetails) {
        loadingService.isLoadin = true;

        console.log(getAddProductDetails);



        inputService.submitAddProductDetails(getAddProductDetails).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);

          if (response.data.errorCode === 0) {
            if ($stateParams.productId =='0') {
                          // toastrMsg(1,'Added new Category!','Successfull..!');
                           // $scope.pop = function(){
                              toaster.pop('success', "Successfull..!", "Added new Product!");
                              // toaster.pop('success', "Added new Category!", "Successfull..!");
                               // };
                             } else{
                               toaster.pop('success', "Successfull..!", "Updated  Product!");

                             }
             // toaster.pop('success', "Successfull..!", "Added New Product!");

            // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
            // console.log($scope.allProducts);
            $window.location.href = '#/admin/product';
          } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
            //  console.log( );
            $window.location.href = '#/login';

          } else {
            // alert(response.data.errorMessage);
            toaster.pop('error', "Oopps..!", response.data.errorMessage);
          }

        });


        /*/fd*/
      }


    }
//GET PRODUCTS
if ($state.current.name === 'admin.product') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
 // /*if (true) {}*/
 //      $scope.noPages = 1;
 //      $scope.getNumber = function(num) {
 //        return new Array(num);
 //      }


      // $scope.pageno = 1;


$scope.productPageVo=
     {"rowPerPage": "15",
       "currentIndex": 0,
       "productName": "",
       "productId": "0",
       "subCategoryId": "0","countryId":0,"cityId":0,"userId":0,
       "categoryId": "0",
       "sellerId": "0",
       "stat": "2",
       "status": []
     }

      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;
      $scope.productFilter = function(search) {
      // alert($scope.pageno);
      loadingService.isLoadin = true;

      // $scope.customers = [];


      console.log(search);
       if (search.categoryId) {
        var categoryId = search.categoryId;
      } else {
        var categoryId = 0;

      }
      if (search.subCategoryId) {
        var subCategoryId = search.subCategoryId;
      } else {
        var subCategoryId = 0;

      }
       if (search.status) {
        var status = search.status;
      } else {
        var status = 2;

      }


$scope.productPageVo=
     {"rowPerPage": "3",
       "currentIndex": 0,
       "productName": "",
       "productId": "0",
       "subCategoryId": subCategoryId,
       "countryId":0,"cityId":0,
       "userId":0,
       "categoryId": categoryId,
       "sellerId": "0",
       "stat": status,
       "status": []
     }

      // $scope.noPages = 1;
      // $scope.getNumber = function(num) {
      //   return new Array(num);
      // }


      // $scope.pageno = 1;
      $scope.getproduct($scope.pageno);

          


    }

      $scope.getproduct = function(pageno) {
        loadingService.isLoadin = true;
        // $scope.productPageVo.currentIndex = (pageno - 1) * $scope.productPageVo.rowPerPage;
          $scope.productPageVo.currentIndex = (pageno - 1) * $scope.productPageVo.rowPerPage;



        inputService.getproducts($scope.productPageVo).then(function(response) {
          loadingService.isLoadin = false;
            $scope.noPages = Math.ceil(response.object.TOTALCOUNTS / $scope.productPageVo.rowPerPage);

          // $scope.noPages = Math.ceil(response.object.TOTALCOUNTS / $scope.productPageVo.rowPerPage);
          loadingService.isLoadin = false;
          console.log(response);
          if (response.errorCode === 0) {
            $scope.products = response.object.Product;
             // $scope.offersonproducts = response.object.Product;

            console.log($scope.products);
            $scope.noproducts=0;
            // console.log($scope.productimages);
          }else if (response.errorCode === -3) {

             $scope.noproducts=1;
               $scope.products =[];

          
          }
        }, function() {
         // alert("Oops! error ode");
          loadingService.isLoadin = false
        });
      };
      $scope.getproduct($scope.pageno);


     $scope.sortproduct=function(sortorder){
      console.log(sortorder);


      $scope.productsortPageVo={
"currentIndex":0,
"mode":sortorder,
"rowPerPage":100
}
console.log( $scope.productsortPageVo);


      $scope.getsortproduct = function(pageno) {
        loadingService.isLoadin = true;
        // $scope.productPageVo.currentIndex = (pageno - 1) * $scope.productPageVo.rowPerPage;
          $scope.productsortPageVo.currentIndex = (pageno - 1) * $scope.productsortPageVo.rowPerPage;



        inputService.getsortproducts($scope.productsortPageVo).then(function(response) {
          loadingService.isLoadin = false;
            $scope.noPages = Math.ceil(response.object.TOTALCOUNTPROD / $scope.productsortPageVo.rowPerPage);

          // $scope.noPages = Math.ceil(response.object.TOTALCOUNTS / $scope.productPageVo.rowPerPage);
          loadingService.isLoadin = false;
          console.log(response);
          if (response.errorCode === 0) {
            $scope.products = response.object.PRODUCTS;
             // $scope.offersonproducts = response.object.Product;

            console.log($scope.products);
            $scope.noproducts=0;
            // console.log($scope.productimages);
          }else if (response.errorCode === -3) {

             $scope.noproducts=1;
               $scope.products =[];

          
          }
        }, function() {
         // alert("Oops! error ode");
          loadingService.isLoadin = false
        });
      };
      $scope.getsortproduct($scope.pageno);



      }
      
         $scope.removeoffertoproduct=function(product){

        console.log(product);
        var productId = product.productId;
        console.log(productId);
        
         // var oferid =0;
         //  console.log(oferid);
          inputService.removeingoffertoproduct(productId).then(function(response) {
         loadingService.isLoadin= false;
        console.log(response);
        if (response.errorCode === 0) {
           // $window.location.href='#/admin/listofproducts';
           $scope.getproduct(1);
           toaster.pop('success', "Successfull..!", "Removed Product from Offer !");
        }
      });
      }




       $scope.viewproduct = function(product) {
        loadingService.isLoadin = true;
        console.log(product);
        $window.location.href = '#/admin/viewproduct/' + product.productId;


        if (product.productId) {
          // alert(product.productId);

          $localStorage.product = product;
          //


        }
      };


      $scope.productmanage = function(product) {

        console.log(product);
        $window.location.href = '#/admin/addproduct/' + product.productId;


        if (product.productId) {
          // alert(product.productId);

          $localStorage.product = product;
          //


        }
      };
        $scope.assignofferonselectedproduct = function(product) {

        console.log(product);
        // alert("haaaa");
        $window.location.href = '#/admin/assignofferonproduct/' + product.productId;


        if (product.productId) {
          // alert(product.productId);

          $localStorage.product = product;
          //


        }
      };
      // (product)
 $scope.listofreviews = function(product) {

        console.log(product);
        $window.location.href = '#/admin/reviews/' + product.productId;


        if (product.productId) {
          // alert(product.productId);

          $localStorage.product = product;
          //


        }
      };
     
     $scope.getallmanufacture = function() {
         loadingService.isLoadin=true;
            $scope.manufacturePageVo =  {
        "manufacturerId":0,
        "manufacturerName":"",

        "status":1,
        "currentIndex":0,
        "rowPerPage":100
}

        inputService.getallmanufactures($scope.manufacturePageVo).then(function(response) {
           loadingService.isLoadin=false;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.manufactures = response.object;

          } else {
           // alert(response.errorMessage);
          }
        });
      }
      $scope.getallmanufacture();
        $scope.getactivecategory = function(maincat) {
        loadingService.isLoadin = true;
        inputService.getactivecategories(maincat).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);
          if (response.errorCode === 0) {
            $scope.categories = response.object;
            console.log($scope.categories);
          }
        });
      };
      $scope.getactivecategory();

    //    $scope.changeCategory = function(categoryId) {
    //   console.log(categoryId);


    //   $scope.getsubcategoryByCategoryId(categoryId)

    // };
    $scope.getsubcategoryByCategoryId = function(categoryId) {
      $scope.subcategoriesbycategories = [];
      inputService.getsubcategoriesByCategoryId(categoryId).then(function(response) {
        console.log(response);
        if (response.errorCode === 0) {
          $scope.subcategoriesbycategories = response.object;
          console.log($scope.subcategoriesbycategories);
        }
      });
    };
 $scope.getsubcategoryByCategoryId()




}


$scope.productSearch= function(productName){
  // alert("enter1");
   // alert($scope.pageno);
  loadingService.isLoadin=true;

  $scope.products=[];


  


  console.log(productName);
  $scope.productPageVo = 
                  {
                      "rowPerPage": "15",
                      "productName": productName,
                       "currentIndex": 0,
                       "productId": "0",
                       "subCategoryId": "0",
                       "countryId":0,
                       "cityId":0,
                       "userId":0,
                       "categoryId": "0",
                       "sellerId": "0",
                       "stat": "2",
                       "status": []
                     }
                     console.log( $scope.productPageVo );
                          // $scope.customerPageVo.currentIndex = (pageno - 1) * $scope.customerPageVo.rowPerPage;                          // alert("hiii");

                inputService.getallproductsbyname($scope.productPageVo).then(function(response) {
                   // alert("enter2");
                  loadingService.isLoadin=false;
                   // $scope.noPages = Math.ceil(response.object.totalRecords / $scope.customerPageVo.rowPerPage);
                    console.log(response);

                    // if (response.errorCode === -3){
                    //  $scope.testdata= true;
                    //     $scope.auctions=[];
                    //    // alert()


                    // }else{
                    //   $scope.testdata= false;
                    // }

                    if (response.errorCode === 0) {
                      // alert("enter3");
                       // $scope.shopList = $scope.shopList.concat(response.object.Product);

                         $scope.productsforrelated = response.object.Product;

                    } else {
                        // alert("enter4");
                        $scope.productsforrelated=[];
                         toaster.pop('error', "Oopps..!", response.errorMessage);
                        // alert(response.errorMessage);
                    }
                },function(response){
                  // alert("enter5");

                    $scope.productsforrelated=[];
                   



                });
            }
            $scope.productSearchwithName= function(productName){
  // alert("enter1");
   // alert($scope.pageno);
  loadingService.isLoadin=true;

  $scope.products=[];


  


  console.log(productName);
  $scope.productPageVo = 
                  {
                      "rowPerPage": "15",
                      "productName": productName,
                       "currentIndex": 0,
                       "productId": "0",
                       "subCategoryId": "0",
                       "countryId":0,
                       "cityId":0,
                       "userId":0,
                       "categoryId": "0",
                       "sellerId": "0",
                       "stat": "2",
                       "status": []
                     }
                     console.log( $scope.productPageVo );
                          // $scope.customerPageVo.currentIndex = (pageno - 1) * $scope.customerPageVo.rowPerPage;                          // alert("hiii");

                inputService.getallproductsbyname($scope.productPageVo).then(function(response) {
                   // alert("enter2");
                  loadingService.isLoadin=false;
                   // $scope.noPages = Math.ceil(response.object.totalRecords / $scope.customerPageVo.rowPerPage);
                    console.log(response);

                    // if (response.errorCode === -3){
                    //  $scope.testdata= true;
                    //     $scope.auctions=[];
                    //    // alert()


                    // }else{
                    //   $scope.testdata= false;
                    // }

                    if (response.errorCode === 0) {
                      // alert("enter3");
                       // $scope.shopList = $scope.shopList.concat(response.object.Product);

                         $scope.products = response.object.Product;
                          $scope.noproducts=0;

                    } else if (response.errorCode=== -3) {

                          $scope.products=[];
                          $scope.noproducts=1;


                    }else{
                        // alert("enter4");
                        $scope.products=[];
                         toaster.pop('error', "Oopps..!", response.errorMessage);
                        // alert(response.errorMessage);
                    }
                },function(response){
                  // alert("enter5");

                    $scope.products=[];
                   



                });
            }

             $scope.productSearchwithNameAr= function(productName){
  // alert("enter1");
   // alert($scope.pageno);
  loadingService.isLoadin=true;

  $scope.products=[];


  


  console.log(productName);
  $scope.productPageVo = 
                  {
                      "rowPerPage": "15",
                      "productName":"",
                      "productNameAr": productName,
                       "currentIndex": 0,
                       "productId": "0",
                       "subCategoryId": "0",
                       "countryId":0,
                       "cityId":0,
                       "userId":0,
                       "categoryId": "0",
                       "sellerId": "0",
                       "stat": "2",
                       "status": []
                     }
                     console.log( $scope.productPageVo );
                          // $scope.customerPageVo.currentIndex = (pageno - 1) * $scope.customerPageVo.rowPerPage;                          // alert("hiii");

                inputService.getallproductsbyname($scope.productPageVo).then(function(response) {
                   // alert("enter2");
                  loadingService.isLoadin=false;
                   // $scope.noPages = Math.ceil(response.object.totalRecords / $scope.customerPageVo.rowPerPage);
                    console.log(response);

                    // if (response.errorCode === -3){
                    //  $scope.testdata= true;
                    //     $scope.auctions=[];
                    //    // alert()


                    // }else{
                    //   $scope.testdata= false;
                    // }

                    if (response.errorCode === 0) {
                      // alert("enter3");
                       // $scope.shopList = $scope.shopList.concat(response.object.Product);

                         $scope.products = response.object.Product;
                          $scope.noproducts=0;

                    } else if (response.errorCode=== -3) {

                          $scope.products=[];
                          $scope.noproducts=1;


                    }else{
                        // alert("enter4");
                        $scope.products=[];
                         toaster.pop('error', "Oopps..!", response.errorMessage);
                        // alert(response.errorMessage);
                    }
                },function(response){
                  // alert("enter5");

                    $scope.products=[];
                   



                });
            }

$scope.deleteselecteditem = function(name) {
        if ( $scope.product.connectedProductsVo.length <= 1) {
          $scope.product.connectedProductsVo.length = [];

        } else {
          for (var i = 0; i <  $scope.product.connectedProductsVo.length; i++) {
            if ( $scope.product.connectedProductsVo[i].uniqueId == name) {
               $scope.product.connectedProductsVo.splice(i, 1);

            }
          }
        }
        //$scope.product.featureOrPhotos[i].data
console.log( $scope.product.connectedProductsVo);
      }

if ($state.current.name === 'admin.stock') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
 // /*if (true) {}*/
 //      $scope.noPages = 1;
 //      $scope.getNumber = function(num) {
 //        return new Array(num);
 //      }


      // $scope.pageno = 1;


$scope.productPageVo=
     {"rowPerPage": "15",
       "currentIndex": 0,
       "productName": "",
       "productId": "0",
       "subCategoryId": "0","countryId":0,"cityId":0,"userId":0,
       "categoryId": "0",
       "sellerId": "0",
       "stat": "2",
       "status": []
     }

      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;

      $scope.getproduct = function(pageno) {
        loadingService.isLoadin = true;
        // $scope.productPageVo.currentIndex = (pageno - 1) * $scope.productPageVo.rowPerPage;
          $scope.productPageVo.currentIndex = (pageno - 1) * $scope.productPageVo.rowPerPage;



        inputService.getproducts($scope.productPageVo).then(function(response) {
          loadingService.isLoadin = false;
            $scope.noPages = Math.ceil(response.object.TOTALCOUNTS / $scope.productPageVo.rowPerPage);

          // $scope.noPages = Math.ceil(response.object.TOTALCOUNTS / $scope.productPageVo.rowPerPage);
          loadingService.isLoadin = false;
          console.log(response);
          if (response.errorCode === 0) {
            $scope.products = response.object.Product;

            console.log($scope.products);
            // console.log($scope.productimages);
          }
        }, function() {
          alert("Oops! error ode");
          loadingService.isLoadin = false
        });
      };
      $scope.getproduct($scope.pageno);

       $scope.viewproduct = function(product) {
        loadingService.isLoadin = true;
        console.log(product);
        $window.location.href = '#/admin/viewproduct/' + product.productId;


        if (product.productId) {
          // alert(product.productId);

          $localStorage.product = product;
          //


        }
      };


      $scope.stockmanage = function(product) {

        console.log(product);
        $window.location.href = '#/admin/addstock/' + product.productId;


        if (product.productId) {
          // alert(product.productId);

          $localStorage.product = product;
          //


        }
      };

     






}
if ($state.current.name === 'admin.addstock') {
$scope.product={};
$scope.product=$localStorage.product;
console.log($scope.product);
$scope.product.productQuantity= $scope.Totalcount;
console.log($scope.product.productQuantity);
  $scope.addProduct = function(getAddProductDetails) {
        loadingService.isLoadin = true;

        console.log(getAddProductDetails);



        inputService.submitAddProductDetails(getAddProductDetails).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);

          if (response.data.errorCode === 0) {
            if ($stateParams.productId =='0') {
                          // toastrMsg(1,'Added new Category!','Successfull..!');
                           // $scope.pop = function(){
                              toaster.pop('success', "Successfull..!", "Successfully Updated Stock!");
                              // toaster.pop('success', "Added new Category!", "Successfull..!");
                               // };
                             } else{
                               toaster.pop('success', "Successfull..!", "Successfully Updated Stock!");

                             }
             // toaster.pop('success', "Successfull..!", "Added New Product!");

            // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
            // console.log($scope.allProducts);
            $window.location.href = '#/admin/stock';
          } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
            //  console.log( );
            $window.location.href = '#/login';

          } else {
            // alert(response.data.errorMessage);
            toaster.pop('error', "Oopps..!", response.data.errorMessage);
          }

        });


        /*/fd*/
      }


  };
$scope.findtotalcount=function(Remaining,newaddingQuantity){
  console.log(Remaining,newaddingQuantity);
  $scope.Totalcount=Remaining+newaddingQuantity;
  console.log($scope.Totalcount);

  // $scope.product.productQuantity= $scope.Totalcount;
  // console.log($scope.product.productQuantity);


}


 $scope.addProductStock=function(getAddProductDetails,newaddingQuantity){
        

         loadingService.isLoadin=true;

      console.log(getAddProductDetails);
      var productId =getAddProductDetails.productId;
      console.log(productId);
      var productQuantity =newaddingQuantity;
      console.log(productQuantity);
                

                inputService.Addstocks(productId,productQuantity).then(function(response) {
                   loadingService.isLoadin=false;
                    console.log(response);
                   
                        if (response.errorCode === 0) {
                          if ($stateParams.categoryId =='0') {
                          // toastrMsg(1,'Added new Category!','Successfull..!');
                           // $scope.pop = function(){
                              toaster.pop('success', "Successfull..!", "Successfully Updated Stock!");
                              // toaster.pop('success', "Added new Category!", "Successfull..!");
                               // };
                             } else{
                               toaster.pop('success', "Successfull..!", "Successfully Updated Stock!");

                             }
                           
                           // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
                           // console.log($scope.allProducts);
                            $window.location.href = '#/admin/stock';
                        } else  if (response.errorCode === 45||response.errorCode === 34) {
                        //  console.log( );
                 $window.location.href = '#/login';

                        }else{
                           toaster.pop('error', "Oopps..!", response.errorMessage);
                          // alert(response.data.errorMessage);
                        }

                });
         

      /*/fd*/
    }


      if ($state.current.name === 'admin.assignofferonproduct') {

      $scope.getalloffer = function() {
            $scope.offerPageVo = {
"offerId":"",
"offerName":"",
"offerNameAr":"",
"status":1,"offerStarts":"","offerEnds":"",

"rowPerPage":"300","currentIndex":0

}
         loadingService.isLoadin=true;

        inputService.getalloffers($scope.offerPageVo).then(function(response) {
           loadingService.isLoadin=false;
          console.log(response);


          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.offers = response.object;
  $scope.nooffers=0;

          }else if (response.errorCode===-3) {
             $scope.nooffers=1;

          } else {
            //alert(response.errorMessage);
          }
        });
      }
      $scope.getalloffer();

       $scope.assignoffertoproduct=function(oferid){

        console.log(oferid);
        var productId =$stateParams.productId;
        console.log(productId);
         // console.log(oferid);
          inputService.assigningoffertoproduct(oferid,productId).then(function(response) {
         loadingService.isLoadin= false;
        console.log(response);
        if (response.errorCode === 0) {
           $window.location.href='#/admin/product';
           // $scope.getproduct(1);
           toaster.pop('success', "Successfull..!", "Assigned Offer to Product!");
        }
      });
      }
      $scope.removeoffertoproduct=function(product){

        console.log(product);
        var productId = product.productId;
        console.log(productId);
        
         // var oferid =0;
         //  console.log(oferid);
          inputService.removeingoffertoproduct(productId).then(function(response) {
         loadingService.isLoadin= false;
        console.log(response);
        if (response.errorCode === 0) {
           // $window.location.href='#/admin/listofproducts';
           $scope.getproduct(1);
           toaster.pop('success', "Successfull..!", "Removed Product from Offer !");
        }
      });
      }

}
  });

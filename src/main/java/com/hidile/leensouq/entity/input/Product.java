package com.hidile.leensouq.entity.input;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;

/**
 * @author user
 *
 */
@Entity
@Table(name = "PRODUCT", uniqueConstraints = { @UniqueConstraint(columnNames = "PRODUCT_ID") })
public class Product implements Serializable 
{

	private static final long serialVersionUID = 4398790091677292595L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PRODUCT_ID", unique = true, nullable = false)
	private long productId;

	@Column(name = "PRODUCT_NAME", unique = false, nullable = false)
	private String productName;
	@ColumnDefault("'NA'")
	@Column(name = "SELLING_UNIT", unique = false, nullable = true)
	private String sellingUnit;

	@Column(name = "PRODUCT_NAME_ARABIC", unique = false, nullable = false)
	private String productNameAr;

	@Column(name = "PRODUCT_QUANTITY", unique = false, nullable = false)
	private double productQuantity;

	@Column(name = "SELLER_ID",unique = false,nullable = false)
	private long sellerId;
	
	@Column(name = "CREATED_DATE", unique = false, nullable = false)
	private long createdDate;

	@Column(name = "LAST_UPDATED_TIME", unique = false, nullable = false)
	private long lastUpdatedTime;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "DESCRIPTION", unique = false, nullable = false)
	private String description;
	
	@Column(name = "SHIPPING_CHARGE", unique = false, nullable = false)
	private double shippingCharge;

	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "DESCRIPTION_ARABIC", unique = false, nullable = false)
	private String descriptionAr;

	@Column(name = "CATEGORY_ID", unique = false, nullable = false)
	private long categoryId;

	@Column(name = "SUBCATEGORY_ID", unique = false, nullable = false)
	private long subCategoryId;

	@Column(name = "MANUFACTURER_ID", unique = false, nullable = false)
	private long manufacturerId;

	@Column(name = "USER_ID", unique = false, nullable = false)
	private long userId;

	@Column(name = "PRICE", unique = false, nullable = false)
	private double price;

	
	
	@Column(name = "TAX_ID", unique = false, nullable = true)
	private long taxId;
	
	

	@Column(name = "RANK", unique = false, nullable = false)
	private int rank;

	@Column(name = "Rating", unique = false, nullable = false)
	private double rating;
	

	@Column(name = "RATING_PERCENTAGE", unique = false, nullable = true)
	private long ratingPercentage;

	@Column(name = "SHARES", unique = false, nullable = false)
	private long shares;

	@Column(name = "STATUS", unique = false, nullable = false)
	private int status;

	@Column(name = "DELIVERY_STATUS", unique = false, nullable = true)
	private int delivaryStatus;
	
	@Column(name = "NO_OF_REVIEWS", unique = false, nullable = false)
	private long noOfReviews;

	@Column(name = "OFFER_ID", unique = false, nullable = false)
	private long offerId;

	@Column(name = "NUMBER_OF_ITEMS", unique = false, nullable = false)
	private double numberOfItems;

	@Column(name = "NUMBER_OF_ITEMS_REMAINING", unique = false, nullable = false)
	private double numberOfItemsRemaining;

	@Column(name = "SERVICE_OR_PRODUCT", unique = false, nullable = false)
	private int serviceOrProduct;
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name ="RETURN_POLICY",unique =false,nullable =true)
	private String returnPolicy;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name ="RETURN_POLICY_AR",unique =false,nullable =true)
	private String returnPolicyAr;
	
	@Column(name = "IS_OUT_OF_STOCK", unique = false, nullable = false)
	private int isOutOfStock;
	
	@ColumnDefault("'00'")
	@Column(name = "COMPANY_ID", unique = false, nullable = true)
	private long companyId;
	
	
	
	

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "SELLER_ID", nullable = true, insertable = false, updatable = false)
	private SellerDetails seller;
	
	
	@Column(name ="WARRANTY",unique =false,nullable =true)
	private String wrranty;
	
	@Column(name ="WARRANTY_AR",unique =false,nullable =true)
	private String wrrantyAr;
	
	
	@Column(name ="INSTALLATION_CHARGE",unique =false,nullable =true)
	private double installationCharge;
	
	@Column(name ="MINIMUM_PURCHASE_LIMIT",unique =false,nullable =true)
	private double minimumPurchaseLimit;
	
	@Column(name ="MINIMUM_PURCHASE_LIMIT_AR",unique =false,nullable =true)
	private double minimumPurchaseLimitAr;

	@Column(name ="UNIT_PRICE_DESCRIPTION_EN",unique =false,nullable =true)
	private String unitPriceDescriptionEn;
	
	@Column(name ="UNIT_PRICE_DESCRIPTION_AR",unique =false,nullable =true)
	private String unitPriceDescriptionAr;
	
	@ColumnDefault("'00'")
	@Column(name = "PURCHASE_PRICE", unique = false, nullable = true)
	private double purchasePrice;
	

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
	private Set<FeaturesAndPhotosProduct> featureAndPhotos;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
	private Set<ConnectedProducts> connectedProducts;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
	private Set<Review> reviews;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
	private Set<ProductAttributes> proudctAttributes;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
	private Set<OfferDetails> offerDetails;
	
	
	
	
	
	
	
	

	public String getSellingUnit() {
		return sellingUnit;
	}

	public void setSellingUnit(String sellingUnit) {
		this.sellingUnit = sellingUnit;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductNameAr() {
		return productNameAr;
	}

	public void setProductNameAr(String productNameAr) {
		this.productNameAr = productNameAr;
	}

	public double getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(double productQuantity) {
		this.productQuantity = productQuantity;
	}

	public long getSellerId() {
		return sellerId;
	}

	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getShippingCharge() {
		return shippingCharge;
	}

	public void setShippingCharge(double shippingCharge) {
		this.shippingCharge = shippingCharge;
	}

	public String getDescriptionAr() {
		return descriptionAr;
	}

	public void setDescriptionAr(String descriptionAr) {
		this.descriptionAr = descriptionAr;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public long getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(long subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public long getManufacturerId() {
		return manufacturerId;
	}

	public void setManufacturerId(long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public long getTaxId() {
		return taxId;
	}

	public void setTaxId(long taxId) {
		this.taxId = taxId;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public long getRatingPercentage() {
		return ratingPercentage;
	}

	public void setRatingPercentage(long ratingPercentage) {
		this.ratingPercentage = ratingPercentage;
	}

	public long getShares() {
		return shares;
	}

	public void setShares(long shares) {
		this.shares = shares;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getDelivaryStatus() {
		return delivaryStatus;
	}

	public void setDelivaryStatus(int delivaryStatus) {
		this.delivaryStatus = delivaryStatus;
	}

	public long getNoOfReviews() {
		return noOfReviews;
	}

	public void setNoOfReviews(long noOfReviews) {
		this.noOfReviews = noOfReviews;
	}

	public long getOfferId() {
		return offerId;
	}

	public void setOfferId(long offerId) {
		this.offerId = offerId;
	}

	public double getNumberOfItems() {
		return numberOfItems;
	}

	public void setNumberOfItems(double numberOfItems) {
		this.numberOfItems = numberOfItems;
	}

	public double getNumberOfItemsRemaining() {
		return numberOfItemsRemaining;
	}

	public void setNumberOfItemsRemaining(double numberOfItemsRemaining) {
		this.numberOfItemsRemaining = numberOfItemsRemaining;
	}

	public int getServiceOrProduct() {
		return serviceOrProduct;
	}

	public void setServiceOrProduct(int serviceOrProduct) {
		this.serviceOrProduct = serviceOrProduct;
	}

	public String getReturnPolicy() {
		return returnPolicy;
	}

	public void setReturnPolicy(String returnPolicy) {
		this.returnPolicy = returnPolicy;
	}

	public String getReturnPolicyAr() {
		return returnPolicyAr;
	}

	public void setReturnPolicyAr(String returnPolicyAr) {
		this.returnPolicyAr = returnPolicyAr;
	}

	public int getIsOutOfStock() {
		return isOutOfStock;
	}

	public void setIsOutOfStock(int isOutOfStock) {
		this.isOutOfStock = isOutOfStock;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public SellerDetails getSeller() {
		return seller;
	}

	public void setSeller(SellerDetails seller) {
		this.seller = seller;
	}

	public String getWrranty() {
		return wrranty;
	}

	public void setWrranty(String wrranty) {
		this.wrranty = wrranty;
	}

	public String getWrrantyAr() {
		return wrrantyAr;
	}

	public void setWrrantyAr(String wrrantyAr) {
		this.wrrantyAr = wrrantyAr;
	}

	public double getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(double installationCharge) {
		this.installationCharge = installationCharge;
	}

	public double getMinimumPurchaseLimit() {
		return minimumPurchaseLimit;
	}

	public void setMinimumPurchaseLimit(double minimumPurchaseLimit) {
		this.minimumPurchaseLimit = minimumPurchaseLimit;
	}

	public double getMinimumPurchaseLimitAr() {
		return minimumPurchaseLimitAr;
	}

	public void setMinimumPurchaseLimitAr(double minimumPurchaseLimitAr) {
		this.minimumPurchaseLimitAr = minimumPurchaseLimitAr;
	}

	public String getUnitPriceDescriptionEn() {
		return unitPriceDescriptionEn;
	}

	public void setUnitPriceDescriptionEn(String unitPriceDescriptionEn) {
		this.unitPriceDescriptionEn = unitPriceDescriptionEn;
	}

	public String getUnitPriceDescriptionAr() {
		return unitPriceDescriptionAr;
	}

	public void setUnitPriceDescriptionAr(String unitPriceDescriptionAr) {
		this.unitPriceDescriptionAr = unitPriceDescriptionAr;
	}

	public double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public Set<FeaturesAndPhotosProduct> getFeatureAndPhotos() {
		return featureAndPhotos;
	}

	public void setFeatureAndPhotos(Set<FeaturesAndPhotosProduct> featureAndPhotos) {
		this.featureAndPhotos = featureAndPhotos;
	}

	public Set<ConnectedProducts> getConnectedProducts() {
		return connectedProducts;
	}

	public void setConnectedProducts(Set<ConnectedProducts> connectedProducts) {
		this.connectedProducts = connectedProducts;
	}

	public Set<Review> getReviews() {
		return reviews;
	}

	public void setReviews(Set<Review> reviews) {
		this.reviews = reviews;
	}

	public Set<ProductAttributes> getProudctAttributes() {
		return proudctAttributes;
	}

	public void setProudctAttributes(Set<ProductAttributes> proudctAttributes) {
		this.proudctAttributes = proudctAttributes;
	}

	public Set<OfferDetails> getOfferDetails() {
		return offerDetails;
	}

	public void setOfferDetails(Set<OfferDetails> offerDetails) {
		this.offerDetails = offerDetails;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
	

	
	
	
	
	
	

}

package com.hidile.leensouq.model.input;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class GetCityVo {
	@NotNull(message = "row per page is null")
	@Min(value = 1, message = "minimum row per page should be 1")
	private int rowPerPage;
	@NotNull(message = " current index  is null")
	@Min(value = 0, message = "minimum current index should be 0")
	private int currentIndex;
	private long cityId;

	private String cityName;

	private int citusStatus;
	private int cityFlag;

	private long countryId;
	private String countryName;
	private String countryNameAr;
	private String cityNameAr;
	
	

	public int getRowPerPage() {
		return rowPerPage;
	}

	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public int getCitusStatus() {
		return citusStatus;
	}

	public void setCitusStatus(int citusStatus) {
		this.citusStatus = citusStatus;
	}

	public int getCityFlag() {
		return cityFlag;
	}

	public void setCityFlag(int cityFlag) {
		this.cityFlag = cityFlag;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryNameAr() {
		return countryNameAr;
	}

	public void setCountryNameAr(String countryNameAr) {
		this.countryNameAr = countryNameAr;
	}

	public String getCityNameAr() {
		return cityNameAr;
	}

	public void setCityNameAr(String cityNameAr) {
		this.cityNameAr = cityNameAr;
	}

}

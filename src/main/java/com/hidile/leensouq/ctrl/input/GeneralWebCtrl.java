package com.hidile.leensouq.ctrl.input;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hidile.leensouq.constants.Constants;
import com.hidile.leensouq.service.CommonService;

@Controller
public class GeneralWebCtrl extends CommonService implements Constants{
	private static final Logger logger = Logger.getLogger(GeneralWebCtrl.class);

//	@Autowired
//	private GeneralService generalService;
	
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView greeting() {
        return new ModelAndView("index");
    }
	
	
	
	/*@RequestMapping(value = GET_VIEW_LOGIN_SCREEN, method = RequestMethod.GET)
    public ModelAndView getLogin() {
		//ModelAndView modelView=;
    //	modelView.addObject("FIRST_URL", INITIAL_URL);
        return new ModelAndView(VIEW_LOGIN_SCREEN);
    }
	
	@RequestMapping(value = SAVE_DEFAULT_ADMIN, method = RequestMethod.GET)
    public ModelAndView addDefaultUser(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException  {
    	logger.info("get here the data");
    	model.addAttribute("FIRST_URL", INITIAL_URL);
    	Map<String, Object> map=new HashMap<>();
    	BasicResponse res=generalService.authenticate("admin", "password");
    	if(res.errorCode != ERROR_CODE_NO_ERROR){
    		AddEmployeeVo addEmp=new AddEmployeeVo();
    		addEmp.address="NA";
    		addEmp.email="sa@k.in";
    		addEmp.name="admin";
    		addEmp.password="password";
    		addEmp.phoneNo="NA";
    		addEmp.status=true;
    		addEmp.rollId=50;
    		res=generalService.saveUser(addEmp);
    	}
    	logger.info(res.getErrorCode());
    	resp.sendRedirect(GENERAL_FIRST_URL);
        return new ModelAndView(VIEW_LOGIN_SCREEN, map);
    }
	
	@RequestMapping(value = GET_VIEW_HOME, method = RequestMethod.GET)
    public ModelAndView callhome(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException {
    	logger.info("get here the data");
    	model.addAttribute("FIRST_URL", INITIAL_URL);
    	Map<String, Object> map=new HashMap<>();
    	if(getLogedUserFromSession(req) == null){
    		resp.sendRedirect(GENERAL_FIRST_URL);
    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
    	}
   		 return new ModelAndView(VIEW_HOME_SCREEN, map);
    }
	
	 @RequestMapping(value = GET_VIEW_ADD_USER, method = RequestMethod.GET)
	    public ModelAndView addUser(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException {
	    	Map<String, Object> map=new HashMap<>();
	    	model.addAttribute("FIRST_URL", INITIAL_URL);
	    	if(getLogedUserFromSession(req) == null){
	    		resp.sendRedirect(GENERAL_FIRST_URL);
	    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
	    	}
	    	if(getLogedUserFromSession(req).roleId == ROLL_ID_ADMIN){  
	      		 return new ModelAndView(VIEW_ADD_USER, map);
	      	     }else{
	      	    	resp.sendRedirect(GENERAL_FIRST_URL+"/home");
	      	    	 return new ModelAndView(VIEW_HOME_SCREEN, map);
	      	     }
	    }
	    
	    @RequestMapping(value = GET_VIEW_USERS, method = RequestMethod.GET)
	    public ModelAndView getUserList(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException {
	    	Map<String, Object> map=new HashMap<>();
	    	model.addAttribute("FIRST_URL", INITIAL_URL);
	    	if(getLogedUserFromSession(req) == null){
	    		resp.sendRedirect(GENERAL_FIRST_URL);
	    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
	    	}
	    	if(getLogedUserFromSession(req).roleId == ROLL_ID_ADMIN){  
	    	return new ModelAndView(VIEW_USERLIST, map);
	    	}else{
	    		resp.sendRedirect(GENERAL_FIRST_URL+"/home");
	    		return new ModelAndView(VIEW_HOME_SCREEN, map);
     	     
	    	}
	    }
	    
	    @RequestMapping(value = GET_VIEW_CUSTOMERS, method = RequestMethod.GET)
	    public ModelAndView getCustomerList(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException {
	    	Map<String, Object> map=new HashMap<>();
	    	model.addAttribute("FIRST_URL", INITIAL_URL);
	    	if(getLogedUserFromSession(req) == null){
	    		resp.sendRedirect(GENERAL_FIRST_URL);
	    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
	    	}
	    	return new ModelAndView(VIEW_CUSTOMERLIST, map);
	    }
	
	
	@RequestMapping(value = "/kerashop", method = RequestMethod.GET)
	public ModelAndView enterToApplication(@RequestParam("username") String user,HttpServletRequest req,HttpServletResponse resp) {
		//BasicResponse res = generalService.authenticate(addUser.customerName, addUser.phoneNumber);
	//	logger.info(res.errorCode);

		if (!user.equals("")) {

			LoggedSession session = new LoggedSession();
			session.userName=user;
			initSession(req, session);
			return new ModelAndView("index");
		} else {
			return new ModelAndView("errorpage");

		}

	}
	*/
	
	
	
}

/**
 * 
 */
package com.hidile.leensouq.ctrl.input;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.hidile.leensouq.constants.Constants;
import com.hidile.leensouq.model.BasicResponse;
import com.hidile.leensouq.service.CommonService;
import com.hidile.leensouq.service.input.InputService;
import com.hidile.leensouq.utils.TimeUtils;

/**
 * @author Shibina EC
 * @email shibina@sociohub.in
 */
@RestController
@RequestMapping("/api")
public class UploadCtrl extends CommonService implements Constants {

	private static final Logger logger = Logger.getLogger(UploadCtrl.class);
	@Autowired
	private InputService inputService;

	@RequestMapping(value = URL_FILE_UPLOAD,headers=("content-type=multipart/*"), method = RequestMethod.POST)
	public BasicResponse upload(@RequestParam("file") MultipartFile file, @RequestParam("type") String type,
			HttpServletRequest req) throws IOException {
		BasicResponse res = new BasicResponse();
		byte[] bytes;
		if (!file.isEmpty()) {
			bytes = file.getBytes();
			String filename = file.getOriginalFilename();
			
			String name = filename.replace(" ", "_");
			logger.info(name+"---->"+filename);
			
			String extension = FilenameUtils.getExtension(filename);
			name = name.replace("." + extension, "");
			String imageNameToSave = name + "_" + TimeUtils.instance().getCurrentTime(0) + "." + extension;

			if (bytes != null) {
				res = ftpFileUploadServer(bytes, imageNameToSave, type);
			}
		}
		return res;
	}

	public BasicResponse ftpFileUploadServer(byte[] bytes, String imageNameToSave, String type) {
		BasicResponse res = null;

		String server = "ftp.sociohub.in";
		int port = 21;
		String user = "hidile@sociohub.in";
		String pass = "London@2016";
		FTPClient ftpClient = new FTPClient();
		try {

			ftpClient.connect(server, port);
			ftpClient.login(user, pass);
			ftpClient.enterLocalPassiveMode();
             
			String firstRemoteFile = "jewelry/" + imageNameToSave;
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			InputStream inputStream1 = new ByteArrayInputStream(bytes);
			System.out.println("Start uploading  file");
			boolean done = ftpClient.storeFile(firstRemoteFile, inputStream1);
			inputStream1.close();
			logger.info(done);
			if (done) {
				res = new BasicResponse();
				System.out.println("The file is uploaded successfully.");
				res.errorCode = 1;
				res.errorMessage = "Success";
				res.setObject("http://sociohub.in/sociohub.in/hidile/jewelry/" + imageNameToSave);
			}else{
				res=new BasicResponse();
				res.errorCode = -3;
				res.errorMessage = "failed";
			}
		} catch (IOException ex) {
			System.out.println("Error: " + ex.getMessage());
			ex.printStackTrace();
			res = new BasicResponse();
			res.errorCode = -5;
			res.errorMessage = ex.getMessage();
		} finally {
			try {
				if (ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return res;

	}
	
	
	@RequestMapping(value = URL_FILE_UPLOAD_TO_AWS,headers=("content-type=multipart/*"), method = RequestMethod.POST)
	public BasicResponse uploadToAWS(@RequestParam("file") MultipartFile file,
			HttpServletRequest req) throws IOException {
		//AWSCredentials credentials = new BasicAWSCredentials("AKIAJEBP6RSKPPAVDA7A", "W89JLYeJqG1Q+C5ueNcgNbDYpmpp1vKsAK/UdXhy");
		AWSCredentials credentials = new BasicAWSCredentials("AKIAILWWYTT3ILLP4Z6A",
                "euIfLAEp590FyXtfyyXgKXRJr1d4OkYsXDAF8sAc");
		
		AmazonS3 s3client = new AmazonS3Client(credentials);
		String bucketName = "projectdatason";
		String folderName="leensouq";
		String filename = file.getOriginalFilename();
		String name = filename.replace(" ", "_");
		logger.info(name+"---->"+filename);
		
		String extension = FilenameUtils.getExtension(filename);
		name = name.replace("." + extension, "");
		String imageNameToSave = "JW_PIC" + "_" + TimeUtils.instance().getCurrentTime(0) + "." + extension;
		String saveLocation = folderName + "/" + imageNameToSave;
		File fileToUpload=multipartToFile(file);
		/*PutObjectResult re=*/s3client.putObject(new PutObjectRequest(bucketName, saveLocation, 
				fileToUpload).withCannedAcl(CannedAccessControlList.PublicRead));
		/*try {
			re.wait();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		BasicResponse res = new BasicResponse();
		logger.info("The file is uploaded successfully.");
		if(fileToUpload.delete()) {
			logger.info("File deleted successfully");
		}
		res.errorCode = 0;
		res.errorMessage = "Success";
		res.setObject(imageNameToSave);
		return res;
	}
	
	public File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException 
	{
	    File convFile = new File( multipart.getOriginalFilename());
	    multipart.transferTo(convFile);
	    return convFile;
	}
	
}

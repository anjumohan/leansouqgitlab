package com.hidile.leensouq.ctrl.user;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hidile.leensouq.constants.Constants;
import com.hidile.leensouq.mail.MailService;
import com.hidile.leensouq.mail.MailVo;
import com.hidile.leensouq.model.BasicResponse;
import com.hidile.leensouq.model.LoggedSession;
import com.hidile.leensouq.service.CommonService;
import com.hidile.leensouq.service.GeneralService;
import com.hidile.leensouq.service.OtpService;

@RestController
@RequestMapping("/api/actv")
public class ActivationCtrl extends CommonService implements Constants {
	private static final Logger logger = Logger.getLogger(GeneralRestCtrl.class);

	@Autowired
	private GeneralService userService;

	@Autowired
	public OtpService otpService;

	@Autowired
	public MailService mailService;

	@RequestMapping(value = FORGET_PASSWORD, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse forgetlogin(@RequestParam("email") String mail, @RequestParam("phoneOrWeb") int phoneOrWeb,
			HttpServletRequest req) 
	{
		BasicResponse res = userService.checkWeatherEmailPresent(mail, ROLL_ID_CUSTOMER, phoneOrWeb);
		String link = null;
		if (res.errorCode == ERROR_CODE_NO_ERROR) 
		{
			LoggedSession session = (LoggedSession) res.object;
			res.object = null;

			int otp = otpService.generateOTP(mail);
			MailVo mailVo = new MailVo();
			mailVo.setMailFrom("info@leensouq.com");
			mailVo.setMailSubject("Password reset link");
			mailVo.setMailTo(mail);
			Map<String, Object> model = new HashMap<String, Object>();
			// model.put("firstName", session.firstName);
			model.put("location", "Jeddah,Saudi Arabia");
			model.put("signature", "Leensouq");
			if (phoneOrWeb == PHONE) 
			{
				StringBuffer textData = new StringBuffer("Dear customer,your otp to reset password is ");
				textData.append(otp);

				String uri = null;
				if (session.countryCode == 91) {
					uri = "http://my.forwardvaluesms.com/vendorsms/pushsms.aspx?apiKey=bcdd5057-0c81-4c17-8d4e-4dff2c0a1958&clientid=455f0161-e64a-4a4c-ac50-1054e9bdfdee&msisdn="
							+ session.countryCode + mail + "&sid=FVALUE&msg=" + textData + "&fl=0";
				} else if (session.countryCode == 966) {
					uri = "http://my.forwardvaluesms.com/vendorsms/pushsms.aspx?apiKey=bcdd5057-0c81-4c17-8d4e-4dff2c0a1958&clientid=455f0161-e64a-4a4c-ac50-1054e9bdfdee&msisdn="
							+ session.countryCode + mail + "&sid=leensouq&msg=" + textData + "&fl=0";
				}
				logger.info(uri);
				RestTemplate restTemplate = new RestTemplate();
				String result = restTemplate.getForObject(uri, String.class);
				ObjectMapper mapper = new ObjectMapper();
				try {
					Map<String, Object> map = mapper.readValue(result, Map.class);
					logger.info(map.get("ErrorCode"));
					if (!map.get("ErrorCode").equals("000")) {
						res.errorCode = 22;
						res.errorMessage = (String) map.get("ErrorMessage");
						return res;
					}
				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				link = REDIRECT_URL + "?email=" + mail + "&key=" + otp;
				model.put("link", link);
				mailVo.setTemplateURL("/templates/emailforgot.vm");
				mailVo.setModel(model);
				mailService.sendEmail(mailVo);
			}
		}

		res.setErrorMessageArabic(link);
		return res;

	}

	@RequestMapping(value = FORGET_OTP_VALIDTE, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse forgetloginValidate(@RequestParam("email") String mail, @RequestParam("otp") String OTP,
			@RequestParam("phoneOrWeb") int phoneOrWeb, HttpServletRequest req) {
		BasicResponse res = new BasicResponse();
		int otp = otpService.getOtp(mail);
		if (otp == Integer.valueOf(OTP)) {
			res = userService.checkWeatherEmailPresent(mail, ROLL_ID_CUSTOMER, phoneOrWeb);
			if (res.errorCode == ERROR_CODE_NO_ERROR) {
				LoggedSession session = (LoggedSession) res.object;
				String compactJws = createToken(session);
				session.token = compactJws;
				res.object = session;
			}
		} else {
			res.errorCode = 1;
			res.errorMessage = "Incorrect otp or expired";
		}
		return res;

	}

	@RequestMapping(value = ACTIVATE_CUSTOMER_LINK, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse activateCustomer(@RequestParam("email") String mail, @RequestParam("otp") String OTP,
			@RequestParam("phoneOrWeb") int phoneOrWeb, HttpServletRequest req) {
		BasicResponse res =new BasicResponse();
		int otp = otpService.getOtp(mail);
		logger.info(otp);
		logger.info("anjuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu" + mail);
		logger.info(Integer.valueOf(OTP));
		if (otp == Integer.valueOf(OTP)) {
			res = userService.checkWeatherEmailPresent(mail, ROLL_ID_CUSTOMER, phoneOrWeb);
			logger.info(res.errorCode);
			if (res.errorCode == ERROR_CODE_NO_ERROR) {
				LoggedSession session = (LoggedSession) res.object;
				res = userService.activateCustomer(session.userId, ACTIVE);
				
				String compactToken=createToken(session);
				logger.info(compactToken+"compactToken__________________________compactToken______________________compactToken_______________compactToken");
				session.token=compactToken;
				
			res.object=session;
				return res;
			}
		}

		else {
		
			res.errorCode = 1;
			res.errorMessage = "Incorrect otp or otp expired";
		}
		return res;

	}

	@RequestMapping(value = RESEND_OTP, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse resendOtp(@RequestParam("email") String mail, @RequestParam("phoneOrWeb") int phoneOrWeb,
			HttpServletRequest req) {
		String headerToken = req.getHeader(TOKEN);
		BasicResponse res = checkTokenOfUser(headerToken);
		if (res.errorCode != ERROR_CODE_NO_ERROR) {
			return res;
		}
		logger.info(mail);
		LoggedSession session1 = (LoggedSession) res.object;

		int countryCode = session1.countryCode;
		logger.info(countryCode);
		logger.info(session1.phone);

		res = userService.checkWeatherEmailPresent(session1.phone, ROLL_ID_CUSTOMER, phoneOrWeb);
		String link = null;
		if (res.errorCode == ERROR_CODE_NO_ERROR) {
			LoggedSession session = (LoggedSession) res.object;
			res.object = null;

			int otp = otpService.generateOTP(mail);
			MailVo mailVo = new MailVo();
			mailVo.setMailFrom("support@pentalsaudi.com");
			mailVo.setMailSubject("Password reset link");
			mailVo.setMailTo(mail);
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("firstName", session.userName);
			model.put("location", "Riyad,Saudi arabia");
			model.put("signature", "www.pentalsaudi.com");
			if (phoneOrWeb == PHONE) {
				StringBuffer textData = new StringBuffer("Dear customer,your otp to verify  is ");
				textData.append(otp);
				String uri = null;
				if (session.countryCode == 91) {
					uri = "http://my.forwardvaluesms.com/vendorsms/pushsms.aspx?apiKey=bcdd5057-0c81-4c17-8d4e-4dff2c0a1958&clientid=455f0161-e64a-4a4c-ac50-1054e9bdfdee&msisdn="
							+ session.countryCode + mail + "&sid=FVALUE&msg=" + textData + "&fl=0";
				} else if (session.countryCode == 966) {
					uri = "http://my.forwardvaluesms.com/vendorsms/pushsms.aspx?apiKey=bcdd5057-0c81-4c17-8d4e-4dff2c0a1958&clientid=455f0161-e64a-4a4c-ac50-1054e9bdfdee&msisdn="
							+ session.countryCode + mail + "&sid=leensouq&msg=" + textData + "&fl=0";
				}
				logger.info(uri);
				RestTemplate restTemplate = new RestTemplate();
				String result = restTemplate.getForObject(uri, String.class);
				ObjectMapper mapper = new ObjectMapper();
				try {
					Map<String, Object> map = mapper.readValue(result, Map.class);
					logger.info(map.get("ErrorCode"));
					if (!map.get("ErrorCode").equals("000")) {
						res.errorCode = 22;
						res.errorMessage = (String) map.get("ErrorMessage");
						return res;
					}
				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				link = REDIRECT_URL + "?email=" + mail + "&key=" + otp;
				model.put("link", link);
				mailVo.setTemplateURL("/templates/emailforgot.vm");
				mailVo.setModel(model);
				mailService.sendEmail(mailVo);
			}
		}

		res.setErrorMessageArabic(link);
		return res;

	}

	@RequestMapping(value = VALIDATE_CUSTOMER_LINK, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public BasicResponse validateCustomer(@RequestParam("email") String mail, @RequestParam("otp") String OTP,
			@RequestParam("phoneOrWeb") int phoneOrWeb, HttpServletRequest req) {
		BasicResponse res = new BasicResponse();
		int otp = otpService.getOtp(mail);
		logger.info(otp);
		logger.info("anjuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu" + mail);
		logger.info(Integer.valueOf(OTP));
		if (otp == Integer.valueOf(OTP)) {
			res = userService.checkWeatherEmailPresent(mail, ROLL_ID_CUSTOMER, phoneOrWeb);
			logger.info(res.errorCode);
			if (res.errorCode == ERROR_CODE_NO_ERROR) {
				LoggedSession session = (LoggedSession) res.object;
				res = userService.validateCustomer(session.userId, ACTIVE);
				return res;
			}
		} else {
			res.errorCode = 1;
			res.errorMessage = "Incorrect otp or otp expired";
		}
		return res;

	}

}

package com.hidile.leensouq.dao.account;

import java.util.List;
import java.util.Map;

import com.hidile.leensouq.entity.account.Expense;
import com.hidile.leensouq.entity.account.Income;
import com.hidile.leensouq.entity.account.IncomeExpenseHead;
import com.hidile.leensouq.entity.order.Transactions;

public interface AccountDao {

	Map<String, Object> saveorUpdateIncomeExpenseHead(IncomeExpenseHead head);

	Map<String, Object> getExpenseIncomeHead(int incomeOrExpense, long incomExpnsHeadId, int status);

	Map<String, Object> saveorUpdateExpense(List<Expense> expenseList, List<Transactions> transList);

	Map<String, Object> saveorUpdateIncome(List<Income> incomeList, List<Transactions> transList);

	Map<String, Object> getIncomes(int currentIndex, int rowPerPage, long incomeId, int status);

	Map<String, Object> getExpenses(int currentIndex, int rowPerPage, long expenseId, int status);

	List<IncomeExpenseHead> getAllIncExpHeadNmaesList(long incomExpnsHeadId);

}

package com.hidile.leensouq.service.report;

import java.io.IOException;
import java.util.Map;

import com.hidile.leensouq.model.BasicResponse;
import com.hidile.leensouq.model.order.OrderNewVos;
import com.hidile.leensouq.model.order.OrderVo;

public interface ReportService {
	
	// BasicResponse getBookingDate( long customerId, int firstIndex, int rowPerPage,long auctionExpiryTime,long auctionTime,int auctionStatus);
	/*public BasicResponse getSubscribeReportFromMode(long fromDate,long toDate,Map<String, String> dataMap,int mode,int status);
	public BasicResponse getBookingDetailsReportFromMode(long fromDate,long toDate,Map<String, String> dataMap,int mode,int status);*/
	//public BasicResponse getAllOrder(long orderId, long sellerId, long customerId,int status[], long fromDate, long toDate, long deliveryDate,long orderListId, int firstIndex, int rowPerPage,long companyId);
	public BasicResponse getAllAdd(int status[], int firstIndex, int rowPerPage,long companyId,long modeId,int stat);
	public BasicResponse getAllSalesReport(long storeId,long customerId,int firstIndex, int rowPerPage,long deliveryBoyId,long categoryId,long productId,long sellerId,long fromDate, long toDate,long orderId,long subCategoryId, long transactionId);
//	public BasicResponse getAllAuctionFrom(long auctionId,long customerId,long auctionDetaiilsId,int firstIndex,int rowPerPage,int status[],long fromDate,long toDate);
//	public BasicResponse getAllAuctionDetailsBy(long auctionId, long auctionDetailsId, long customerId,int firstIndex,int rowPerPage,long fromDate,long toDate);
	public BasicResponse  getAllTransBy(long mode,long fromDate,long toDate,long toWhoId,int cashType,int tranType,int currentIndex,int rowPerPage);
	public Map<Integer, String> getAllTransMap();
	 public BasicResponse addOrder(OrderNewVos orderVo);
	public BasicResponse getAllOrder(long orderId, long storeId, long customerId, int firstIndex, int rowPerPage,
			int status[], long fromDate, long toDate, long deliveryDate, int serviceOrProduct, long deliveryBoyId,
			long orderListId)throws IOException;
	public BasicResponse monthlyRevenueReport(int firstIndex,int rowPerPage,long fromDate,long toDate,long codeId);
	public BasicResponse yearlyRevenueReport(int firstIndex,int rowPerPage,long fromDate,long toDate,long codeId);
	public BasicResponse weeklyRevenueReport(int firstIndex,int rowPerPage,long fromDate,long toDate,long codeId);
	 public BasicResponse getAllDetails(long userId,int role);
	 public BasicResponse getAllSalesReport(long storeId, long customerId, int firstIndex, int rowPerPage,
				long deliveryBoyId, long categoryId, long productId, long sellerId, long fromDate, long toDate,
				long subCategoryId, int tranMode);
	 public BasicResponse  bestPurchasedProducts(long orderId,long orderListId,long customerId,long fromDate,long toDate,long sellerId,int firstIndex,int rowPerPage,long companyId, long categoryId,long subCategoryId);
	 public BasicResponse getAllOrdersDetails(long orderId,long orderListId,long customerId,long fromDate,long toDate,long sellerId,int firstIndex,int rowPerPage,long companyId);
	public BasicResponse lowStockReport(int firstIndex,int rowPerPage,int status,double numberOfItemsRemaining,long numberStarts,long numberEnds);
	public BasicResponse transactionReport(int firstIndex,int rowPerPage,int status,long lastUpdatedTime,long orderId,long userId, double priceStarts,double priceEnds, long fromDate, long toDate,int transactionMode,int tMode[]);
	public BasicResponse getPromocodeReport(int firstIndex,int rowPerPage,long fromDate,long toDate,long codeId,long userId,long promoterId);
	public BasicResponse getAllProduct(long productId,long sellerId,long userId,long mainCategoryId,long categoryId,long subcategoryId,long countryId,long cityId,int firstIndex,int rowPerPage,int status,long companyId,double numberOfItemsRemaining,long offerId,long manufacturerId,double priceStarts,double priceEnds,long productAttributeId);
	//public BasicResponse productPurchaseReport(int firstIndex,int rowPerPage);
}

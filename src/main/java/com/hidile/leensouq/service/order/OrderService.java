package com.hidile.leensouq.service.order;

import java.util.List;
import java.util.Map;

import com.hidile.leensouq.entity.order.OrderDetails;
import com.hidile.leensouq.model.BasicResponse;
import com.hidile.leensouq.model.input.CityVo;
import com.hidile.leensouq.model.input.CountryVo;
import com.hidile.leensouq.model.input.ReturnItemsVo;
import com.hidile.leensouq.model.order.CardDetailsVo;
import com.hidile.leensouq.model.order.OrderVo;
import com.hidile.leensouq.model.order.ShippingVo;
import com.hidile.leensouq.model.order.TaxVo;


public interface OrderService
{

//	 public BasicResponse saveOrUpdateAuction(AuctionVo auctionVo);
	
	 //public BasicResponse addOrder(OrderVo orderVo);
	 public BasicResponse saveOrUpdateTax(TaxVo taxVo);
	 public BasicResponse getTax(long countryId,long cityId,long taxId,int rowPerPage,int currentIndex,int stat);
	 public BasicResponse getAllShippingDetails(long customerId,long shippingId,int rowPerPage,int currentIndex,int stat);
	 public BasicResponse   getCardDetails(long cardId,long customerId,int currentIndex,int rowPerPage);
	// public BasicResponse  saveOrUpdateCart(CartAndFavouritesVo cartAndFavouritesVo);
	 public BasicResponse deletetax(long taxId);
//	 public BasicResponse  saveAuctionDetails(AuctionDetailsVo acDetailsVo);
	 public BasicResponse saveOrUpdateCountry(CountryVo countryVo);
	 public BasicResponse saveOrUpdateCity(CityVo cityVo);
	
	 public BasicResponse getShippingByOrder(long orderId);
	 public BasicResponse getAllReturnProducts(long returnItemId,int rowPerPage,int currentIndex,long storeId,int stat);
//	public BasicResponse deleteAuctionDetails(long auctionId,long auctionDetailsId);
	 public BasicResponse changeStatusOfOrder(int status,long orderId,long userId,List<Long> deleteOrderListIds);
	public BasicResponse addShippingDetails(ShippingVo shippingVo);
	public BasicResponse addCardDetails(CardDetailsVo shippingVo);
//	public BasicResponse changeStatusOfAuction(int status,long auctionId,long userId);
	//public BasicResponse getAllAuctionByBid(long auctionId);
 	public BasicResponse addReturnProduct(List<ReturnItemsVo> returnItemsVos);
 	 public Map<Long, String> getAllCountryNames();
 	public Map<Integer, String> getAllOrderStatus();
	public Map<Integer, String> getAllOrderStatusAr();
 	  public BasicResponse deleteShipping(long shippingId);
 	 public BasicResponse deleteCard(long cardId);
}

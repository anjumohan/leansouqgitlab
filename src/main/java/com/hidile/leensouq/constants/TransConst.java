package com.hidile.leensouq.constants;

public interface TransConst {
     		
     		
     		
     	//	------------------ PAYMENT MODES-------------------------------------------
     		
     		public static final int CASH_IN = 1;
     		public static final int CASH_OUT = 0;
     		
     		public static final int CLEAR_TRAN = 1;
     		public static final int NOT_CLEAR_TRAN = 0;
     		
     		
     		public static final int MOVED= 15;
     		public static final int NOT_MOVED = 16;
     		
     		public static final int TRAN_MODE_DEBIT_SALE = 20;
     		public static final int TRAN_MODE_DEBIT_CASH_PAY = 21;
     		public static final int TRAN_MODE_DEBIT_ONLINE_PAY = 22;
     		public static final int TRAN_MODE_CREDIT_RETURN_PAY = 23;
     		public static final int TRAN_MODE_CREDIT_CANCEL_PAY = 24;
     		
     		public static final String TRAN_MODE_DEBIT_SALE_STR="DEBIT SALE";
     		public static final String TRAN_MODE_DEBIT_CASH_PAY_STR = "COD PAY";
     		public static final String TRAN_MODE_DEBIT_ONLINE_PAY_STR = "ONLINE PAY";  
     		//*****************************to get Details****************
     		public static final int TRAN_CASH=201;
     		public static final int TRAN_CARD=202;
     		public static final int TRAN_CHEQUE=203;
     		public static final int TRAN_BANK=204;
     		//....................................................purchase return
     		public static final int TRANS_MODE_SALE_RETURN=250;
     		public static final int TRANS_MODE_SALE_RETURN_SUPPLIER=251;
     		
     		public static final String TRANS_MODE_SALE_RETURN_STR="Purchase return";
}
